import { Base_API } from "../_constants/matcher";
import axios from "axios";

export const bannerAction = {
  InsertBanner,
  UpdateBanner,
  GetBannerByID,
  DeleteBanner
};

function InsertBanner(formData) {
    return axios.post(`${Base_API.url_api}/backend/InsertBanner`, formData).then(res => {
        return res.data;
    });
}

function UpdateBanner(formData, id) {
    return axios.put(`${Base_API.url_api}/backend/UpdateBanner/${id}`, formData).then(res => {
        return res.data;
    });
}

function GetBannerByID(id) {
    return axios.get(`${Base_API.url_api}/backend/GetBannerByID/${id}`, "").then(res => {
        return res.data;
    });
}

function DeleteBanner(id) {
    return axios({
      method: "DELETE",
      url: `${Base_API.url_api}/backend/DeleteBanner/${id}`
    })
      .then(function(response) {
        return response.data;
      })
      .catch(function(response) {
        console.log(response);
      });
}