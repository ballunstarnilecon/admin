import { Base_API } from "../_constants/matcher";
import axios from "axios";
const api_key = "YTPflRATmjiZGXo5L3eouYIRcmKrtcnE";
export const adminAction = {
    checkLogin,
    InsertAdmin,
    updateAdmin,
    getAdmin,
    deleteAdmin
};

// function checkLogin(inputData) {
//   const headers = {
//     'Content-Type': 'application/json',
//     'API-KEY': api_key
//   }
//   return axios.post(`${Base_API.url_api}/backend/admin/checkUser`, inputData,  {
//     headers: headers
//   }).then(res => {
//     return res.data;
//   });
// }
function checkLogin (data){
  return axios.post(`${Base_API.url_api}/backend/admin/checkUser`,data).then(res => {
      return res;
  });
}
function InsertAdmin(data) {
  return axios.post(`${Base_API.url_api}/backend/admin/InsertAdmin`,data).then(res => {
    return res;
});
}

// function updateAdmin(data) {
//   return axios.post(`${Base_API.url_api}/backend/admin/updateAdmin`,data).then(res => {
//     return res;
// });
// }

function updateAdmin(formData, id) {
  return axios.put(`${Base_API.url_api}/backend/admin/UpdateAdmin/${id}`, formData).then(res => {
    return res.data;
  });
}

function deleteAdmin(id) {
  return axios({
    method: "DELETE",
    url: `${Base_API.url_api}/backend/admin/DeleteAdmin/${id}`
  })
    .then(function(response) {
      return response.data;
    })
    .catch(function(response) {
      console.log(response);
    });
}

function getAdmin(id) {
  return axios.get(`${Base_API.url_api}/backend/admin/GetAdminByID/${id}`).then(res => {
    return res;
});
}
