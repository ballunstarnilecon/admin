import { Base_API } from "../_constants/matcher";
import axios from "axios";

export const movieAction = {
  InsertMovie,
  UpdateMovie,
  GetMovieByID,
  DeleteMovie
};

function InsertMovie(formData) {
    return axios.post(`${Base_API.url_api}/backend/InsertMovie`, formData).then(res => {
        return res.data;
    });
}

function UpdateMovie(formData, id) {
    return axios.put(`${Base_API.url_api}/backend/UpdateMovie/${id}`, formData).then(res => {
        return res.data;
    });
}

function GetMovieByID(id) {
    return axios.post(`${Base_API.url_api}/backend/GetMovieByID/${id}`, "").then(res => {
        return res.data;
    });
}

function DeleteMovie(id) {
    return axios({
      method: "DELETE",
      url: `${Base_API.url_api}/backend/DeleteMovie/${id}`
    })
      .then(function(response) {
        return response.data;
      })
      .catch(function(response) {
        console.log(response);
      });
}