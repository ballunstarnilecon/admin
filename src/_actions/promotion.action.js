import { Base_API } from "../_constants/matcher";
import axios from "axios";

export const promotionAction = {
  InsertPromotion,
  UpdatePromotion,
  GetPromotionByID,
  DeletePromotion
};

function InsertPromotion(formData) {
    return axios.post(`${Base_API.url_api}/backend/InsertPromotion`, formData).then(res => {
        return res.data;
    });
}

function UpdatePromotion(formData, id) {
    return axios.put(`${Base_API.url_api}/backend/UpdatePromotion/${id}`, formData).then(res => {
        return res.data;
    });
}

function GetPromotionByID(id) {
    return axios.get(`${Base_API.url_api}/backend/GetPromotionByID/${id}`, "").then(res => {
        return res.data;
    });
}

function DeletePromotion(id) {
    return axios({
      method: "DELETE",
      url: `${Base_API.url_api}/backend/DeletePromotion/${id}`
    })
      .then(function(response) {
        return response.data;
      })
      .catch(function(response) {
        console.log(response);
      });
}