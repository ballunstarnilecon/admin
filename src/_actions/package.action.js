import { Base_API } from "../_constants/matcher";
import axios from "axios";
const api_key = "YTPflRATmjiZGXo5L3eouYIRcmKrtcnE";

export const packageAction = {
    listPackageCategory,
    createPackageCategory,
    updatePackageCategory,
    getPackageCategory,
    deletePackageCategory,

    createPackage,
    updatePackage,
    getPackage,
    deletePackage,

    createPackageRecommend,
    updatePackageRecommend,
    getPackageRecommend,
    deletePackageRecommend,
};

function listPackageCategory(lang) {
    const headers = {
        'API-KEY': api_key
    }
    return axios.get(`${Base_API.url_api}/api/backend/packageCategory/${lang}?v=1.1`, {
        headers: headers
      }).then(res => {
        return res.data;
    });
}

function createPackageCategory(formData, lang) {
    const headers = {
        'API-KEY': api_key
    }
    return axios.post(`${Base_API.url_api}/api/backend/packageCategory/${lang}?v=1.1`, formData, {
        headers: headers
      }).then(res => {
        return res.data;
    });
}

function updatePackageCategory(formData, id, lang) {    
    const headers = {
        'API-KEY': api_key
    }
    return axios.put(`${Base_API.url_api}/api/backend/packageCategory/${lang}/${id}?v=1.1`, formData, {
        headers: headers
      }).then(res => {
        return res.data;
    });
}

function getPackageCategory(id, lang) {
    const headers = {
        'API-KEY': api_key
    }
    return axios.get(`${Base_API.url_api}/api/backend/packageCategory/${lang}/${id}?v=1.1`, {
        headers: headers
      }).then(res => {
        return res.data;
    });
}

function deletePackageCategory(id, lang) {
    const headers = {
        'API-KEY': api_key
    }
    return axios.delete(`${Base_API.url_api}/api/backend/packageCategory/${lang}/${id}?v=1.1`,
        { headers: headers }
    )
    .then(function(res) {
        return res.data;
    })
    .catch(function(res) {
        console.log(res);
    });
}

function createPackage(formData, lang) {
    const headers = {
        'API-KEY': api_key
    }
    return axios.post(`${Base_API.url_api}/api/backend/package/${lang}?v=1.1`, formData,{
        headers: headers
      }).then(res => {
        return res.data;
    });
}

function updatePackage(formData, id, lang) {    
    const headers = {
        'API-KEY': api_key
    }
    return axios.put(`${Base_API.url_api}/api/backend/package/${lang}/${id}?v=1.1`, formData,{
        headers: headers
      }).then(res => {
        return res.data;
    });
}

function getPackage(id, lang) {
    const headers = {
        'API-KEY': api_key
    }
    return axios.get(`${Base_API.url_api}/api/backend/package/${lang}/${id}?v=1.1`, {
        headers: headers
      }).then(res => {
        return res.data;
    });
}

function deletePackage(id, lang) {
    const headers = {
        'API-KEY': api_key
    }
    return axios.delete(`${Base_API.url_api}/api/backend/package/${lang}/${id}?v=1.1`,
        { headers: headers }
    )
    .then(function(res) {
        return res.data;
    })
    .catch(function(res) {
        console.log(res);
    });
}

function createPackageRecommend(formData, lang) {
    const headers = {
        'API-KEY': api_key
    }
    return axios.post(`${Base_API.url_api}/api/backend/packageRecommend/${lang}?v=1.1`, formData, {
        headers: headers
      }).then(res => {
        return res.data;
    });
}

function updatePackageRecommend(formData, id, lang) {    
    const headers = {
        'API-KEY': api_key
    }
    return axios.put(`${Base_API.url_api}/api/backend/packageRecommend/${lang}/${id}?v=1.1`, formData, {
        headers: headers
      }).then(res => {
        return res.data;
    });
}

function getPackageRecommend(id, lang) {
    const headers = {
        'API-KEY': api_key
    }
    return axios.get(`${Base_API.url_api}/api/backend/packageRecommend/${lang}/${id}?v=1.1`, {
        headers: headers
      }).then(res => {
        return res.data;
    });
}

function deletePackageRecommend(id, lang) {
    const headers = {
        'API-KEY': api_key
    }
    return axios.delete(`${Base_API.url_api}/api/backend/packageRecommend/${lang}/${id}?v=1.1`,
        { headers: headers }
    )
    .then(function(res) {
        return res.data;
    })
    .catch(function(res) {
        console.log(res);
    });
}