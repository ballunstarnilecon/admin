import { Base_API } from "../_constants/matcher";
import axios from "axios";

export const newsAction = {
  InsertNews,
  UpdateNews,
  GetNewsByID,
  DeleteNews
};

function InsertNews(formData) {
    return axios.post(`${Base_API.url_api}/backend/InsertNews`, formData).then(res => {
        return res.data;
    });
}

function UpdateNews(formData, id) {
    return axios.put(`${Base_API.url_api}/backend/UpdateNews/${id}`, formData).then(res => {
        return res.data;
    });
}

function GetNewsByID(id) {
    return axios.get(`${Base_API.url_api}/backend/GetNewsByID/${id}`, "").then(res => {
        return res.data;
    });
}

function DeleteNews(id) {
    return axios({
      method: "DELETE",
      url: `${Base_API.url_api}/backend/DeleteNews/${id}`
    })
      .then(function(response) {
        return response.data;
      })
      .catch(function(response) {
        console.log(response);
      });
}