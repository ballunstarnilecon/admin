import { Base_API } from "../_constants/matcher";
import axios from "axios";

export const roundMovieAction = {
  ListMovieShowtimes,
  InsertMovieShowtimes
};

function ListMovieShowtimes() {
  return axios.get(`${Base_API.url_api}/ListMovieShowtimes`).then(res => {
      return res.data;
  });
}

function InsertMovieShowtimes() {
    return axios.post(`${Base_API.url_api}/InsertMovieShowtimes`).then(res => {
        return res.data;
    });
}