import React from 'react';
import { Route, Redirect } from 'react-router-dom';

export const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        localStorage.getItem('username-embassy')
            ? <Component {...props} />
            : <Redirect to={{ pathname: '/backoffice/login', state: { from: props.location } }} />
    )} />
)