import React from "react";
import { bannerAction } from "../../../_actions";
import SweetAlert from "react-bootstrap-sweetalert";
import Select from "react-select";
import "../banner.css";
import { SubHeader } from "../../../_pagebuilder";

let modalAlert;

export class Banner_Add extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      show: false,
      modal: null,
      selectOption: [],
      fields: {
      },
      errors: {},
      lang: "th",
      active: "ManageBanner",
      is_active: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onConfirm = this.onConfirm.bind(this);
    this.onConfirmCreate = this.onConfirmCreate.bind(this);
    this.onCancel = this.onCancel.bind(this);
  }

  componentDidMount() {

  }

  onModalSuccess(head, body) {
    modalAlert = () => (
      <SweetAlert
        style={{ display: "block" }}
        success
        confirmBtnBsStyle="success"
        cancelBtnBsStyle="default"
        title={head}
        onConfirm={this.onConfirm}
      >
        {body}
      </SweetAlert>
    );

    this.setState({ show: true, modal: modalAlert() });
  }

  onModalWarning(head, body) {
    modalAlert = () => (
      <SweetAlert
        style={{ display: "block" }}
        warning
        confirmBtnBsStyle="success"
        cancelBtnBsStyle="default"
        title={head}
        onConfirm={this.onCancel}
      >
        {body}
      </SweetAlert>
    );

    this.setState({ show: true, modal: modalAlert() });
  }

  onModalError(head, body) {
    modalAlert = () => (
      <SweetAlert
        style={{ display: "block" }}
        error
        confirmBtnBsStyle="success"
        cancelBtnBsStyle="default"
        title={head}
        onConfirm={this.onCancel}
      >
        {body}
      </SweetAlert>
    );

    this.setState({ show: true, modal: modalAlert() });
  }

  onModalQuestion(head, body) {
    modalAlert = () => (
      <SweetAlert
        style={{ display: "block" }}
        info
        showCancel
        confirmBtnBsStyle="success"
        cancelBtnBsStyle="default"
        title={head}
        onConfirm={() => this.onConfirmCreate()}
        onCancel={() => this.onCancel()}
      >
        {body}
      </SweetAlert>
    );

    this.setState({ show: true, modal: modalAlert() });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({ modalOption: null });

    if (this.validateForm()) {
      this.onModalQuestion("Confirm", "คุณต้องการเพิ่มข้อมูล ?");
    }
  }

  onConfirm() {
    this.setState({ modal: null });
    window.location.href = "/backoffice/banner";
  }

  validateForm() {
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    if (!fields["banner_image"]) {
      formIsValid = false;
      errors["banner_image"] = "*กรุณาแนบรูปแบนเนอร์";
    }

    this.setState({
      errors: errors,
    });

    return formIsValid;
  }

  onConfirmCreate(ev) {
    this.setState({ modal: null });

    var formData = new FormData();
    formData.append("banner_image",this.state.fields["banner_image"]);
    formData.append("banner_sort",this.state.fields["banner_sort"] ? this.state.fields["banner_sort"] : 0);
    formData.append("is_active", this.state.is_active == true ? '1' : '0');

    // for (var value of formData.values()) {
    //   console.log(value);
    // }

    bannerAction.InsertBanner(formData).then((e) => {
      if (e.isSuccess === true) {
        this.onModalSuccess("Success", e.message);
      } else {
        this.onModalError("Error", e.message);
      }
    });
  }

  onCancel(ev) {
    this.setState({ modal: null });
  }


  onHandleCheckChar = (e) =>{
    if(!e.target.value.match(/^[+ฯะัาำิีึืฺุูเแโใไๅๆ็่้๊๋์,ก-ฮa-zA-Z0-9.\s:#/_-]+$/i) && e.target.value.length>0){
       this.onModalError('ไม่สามารถใช้ตัวอักษรพิเศษได้',e.message)
      e.target.value = e.target.value.toString();
    }else{
      return this.handleChange(e)
    }
  }

  handleChange = (e) => {
    if(!e.target.value.match(/^[+ฯะัาำิีึืฺุูเแโใไๅๆ็่้๊๋์,ก-ฮa-zA-Z0-9.\s:#/_-]+$/i) && e.target.value.length>0){
      this.onModalError('ไม่สามารถใช้ตัวอักษรพิเศษได้',e.message)
     e.target.value = e.target.value.toString();
   }else{
    let { fields, errors } = this.state;
    fields[e.target.name] = e.target.value;
    this.state.errors[e.target.name] = null;
    this.setState({ fields, errors });
   }
  };

  getExtension(filename) {
    var parts = filename.split(".");
    return parts[parts.length - 1];
  }

  isImage = (e) => {
    var ext = this.getExtension(e.target.files[0].name);
    switch (ext.toLowerCase()) {
      case "jpg":
      case "jpeg":
      case "png":
        //etc
        // return this.onChangeUploadHandler(e);
        return this.onChangeUploadHandler(e)
    }
    return this.onModalError('.jpg , .jpeg , .png Only',e.message);
    
  };

  onChangeUploadHandler(e) {
    let { fields, errors } = this.state;
    fields[e.target.name] = e.target.files[0];
    this.state.errors[e.target.name] = null;
    this.setState({
      fields,
      errors,
    });
  }

  handleSelectChange = (e, action) => {
    let { fields, errors } = this.state;
    fields[action.name] = e.value;
    this.state.errors[action.name] = null;
    this.setState({ fields, errors });
  };

  handleInputChange = (e) => {
    const target = e.target;
    const name = target.name;
    if (name == "is_active") {
      this.setState({ is_active: !this.state.is_active });
    }
  };

  render() {

    return (
      <div>
        <SubHeader
          main_menu_name="จัดการแบนเนอร์"
          main_menu_link=""
          sub_menu_name="รายการแบนเนอร์"
          sub_menu_link="/backoffice/banner"
          sub_menu_function="เพิ่มแบนเนอร์"
        />

        <div className="content">
          <div className="card">
            <div className="card-header header-elements-inline">
              <div className="heading-elements">
                <h4 className="panel-title">เพิ่มแบนเนอร์</h4>

                <div className="card-body">
                  <form
                    className="form-horizontal"
                    name="FormCreate"
                    onSubmit={(e) => this.handleSubmit(e)}
                  >
                    <fieldset className="content-group">
                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                          รูปภาพแบนเนอร์{" "}
                          <div className="text-danger"></div>
                        </label>

                        <div className="col-lg-10">
                          <input
                            type="file"
                            className="file-input"
                            data-show-upload="false"
                            autoOrientImage="false"
                            name="banner_image"
                            onChange={this.isImage}
                            accept=".jpg , .png ,.jpeg"
                          />
                          <div className="errorMsg">
                            {this.state.errors.banner_image}
                          </div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                          ลำดับการแสดง
                        </label>
                        <div className="col-lg-4">
                          <input
                            name="banner_sort"
                            type="number"
                            className="form-control"
                            placeholder="ลำดับการแสดง"
                            value = {this.state.fields["banner_sort"] || ""}
                            onChange={this.onHandleCheckChar}
                          />
                          <div className="errorMsg">
                            {this.state.errors.banner_sort}
                          </div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                          สถานะการแสดง
                        </label>
                        <div className="col-lg-10">
                          <label className="switch">
                            <input
                              name="is_active"
                              type="checkbox"
                              onChange={this.handleInputChange}
                              checked={this.state.is_active}
                            />
                            <div className="slider"></div>
                          </label>
                        </div>
                      </div>

                      <div className="form-group row">
                        <div className="col-lg-12 text-right">
                          <input
                            type="submit"
                            className="btn btn-success btn-md btn-create"
                            value="บันทึก"
                          />
                        </div>
                      </div>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
            {this.state.modal}
          </div>
        </div>
      </div>
    );
  }
}
