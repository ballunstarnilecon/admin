import React from "react";
import { bannerAction } from '../../../_actions'
import SweetAlert from 'react-bootstrap-sweetalert';
import "../banner.css";
import { SubHeader} from "../../../_pagebuilder";
import moment from "moment"
import 'moment/locale/th';
import Select from "react-select";

let modalAlert;

export class Banner_Edit extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      show: false,
      modal: null,
      selectOption: [],
      fields: {},
      errors: {},
      lang: 'th'
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onConfirm = this.onConfirm.bind(this);
    this.onConfirmUpdate = this.onConfirmUpdate.bind(this);
    this.onCancel = this.onCancel.bind(this);
  }

  componentDidMount() {
    let { selectOption } = this.state;

    const {
      match: { params }
    } = this.props;
    this.getBannerByID(params.banner_id);
  }

  getBannerByID(banner_id) {
    
    bannerAction.GetBannerByID(banner_id).then(e => {
      if (e.isSuccess === true) {
        let data = e.data[0]
        this.setState({
          fields: {
            ...this.state.fields,
            "banner_id": data.banner_id,
            "banner_image": data.banner_image,
            "banner_image_show": data.banner_image,
            "banner_sort": data.banner_sort,
            "is_active": data.is_active,
            "updated_at": moment(data.updated_at,'YYYY-MM-DD , HH:mm:ss').locale('th').format('DD/MM/YYYY HH:mm:ss')
          },
          "is_active": data.is_active,
        });
      
      } else {
        this.onModalError("Error", "ไม่พบข้อมูลดังกล่าว");
      }
    });
  }

  onModalSuccess(head, body) {
    modalAlert = () => (
      <SweetAlert
          style={{display:'block'}}
          success
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="default"
          title={head}
          onConfirm={this.onConfirm}
          >
          {body}
          </SweetAlert>
      );

      this.setState({ show:true, modal: modalAlert() })
  }

  onModalWarning(head, body) {
    modalAlert = () => (
      <SweetAlert
          style={{display:'block'}}
          warning
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="default"
          title={head}
          onConfirm={this.onCancel}
          >
          {body}
          </SweetAlert>
      );

      this.setState({ show:true, modal: modalAlert() })
  }

  onModalError(head, body) {
    modalAlert = () => (
      <SweetAlert
          style={{display:'block'}}
          error
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="default"
          title={head}
          onConfirm={this.onCancel}
          >
          {body}
          </SweetAlert>
      );

      this.setState({ show:true, modal: modalAlert() })
  }

  onModalQuestion(head, body) {
    modalAlert = () => (
    <SweetAlert
        style={{display:'block'}}
        info
        showCancel
        confirmBtnBsStyle="success"
        cancelBtnBsStyle="default"
        title={head}
        onConfirm={() => this.onConfirmUpdate()}
        onCancel={() => this.onCancel()}
        >
        {body}
        </SweetAlert>
    );

    this.setState({ show:true, modal: modalAlert() })
  }


  handleSubmit(event) {
    event.preventDefault()
    this.setState({ modalOption: null })

    if (this.validateForm()) {
      this.onModalQuestion('Confirm','คุณต้องการแก้ไขข้อมูล ?')
    }
  }

  onConfirm () {
    this.setState({ modal: null })
    window.location.href="/backoffice/banner"
  }

  validateForm() {

    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    if (!fields["banner_image"]) {
      formIsValid = false;
      errors["banner_image"] = "*กรุณาแนบรูปแบนเนอร์";
    }

    this.setState({
      errors: errors
    });
    return formIsValid;
  }

  onConfirmUpdate(ev) {
    this.setState({ modal: null })
    let id = this.state.fields['banner_id'];
    
    var formData = new FormData();
    formData.append("banner_image",this.state.fields["banner_image"]);
    formData.append("banner_sort", this.state.fields["banner_sort"]);
    formData.append("is_active", this.state.is_active == true ? 1 : 0);

    bannerAction.UpdateBanner(formData, id).then(e => {
      if(e.isSuccess === true) {
        this.onModalSuccess('Success', e.message)
      } else {
        this.onModalError('Error', e.message)
      }
    })
  }

  onCancel(ev) {
    this.setState({ modal: null });
  }

  onHandleCheckChar = (e) =>{
    if(!e.target.value.match(/^[+ฯะัาำิีึืฺุูเแโใไๅๆ็่้๊๋์,ก-ฮa-zA-Z0-9.:\s#/_-]+$/i) && e.target.value.length>0){
      this.onModalError('ไม่สามารถใช้ตัวอักษรพิเศษได้',e.message)
      e.target.value = e.target.value.toString();
    }else{
      return this.handleChange(e)
    }
  }

  handleChange = (e) => {
    if(!e.target.value.match(/^[+ฯะัาำิีึืฺุูเแโใไๅๆ็่้๊๋์,ก-ฮa-zA-Z0-9.:\s#/_-]+$/i) && e.target.value.length>0){
      this.onModalError('ไม่สามารถใช้ตัวอักษรพิเศษได้',e.message)
      e.target.value = e.target.value.toString();
    }else{
      let { fields, errors } = this.state;
      fields[e.target.name] = e.target.value;
      this.state.errors[e.target.name] = null;
      this.setState({ fields, errors });
    }
  };

  getExtension(filename) {
    var parts = filename.split(".");
    return parts[parts.length - 1];
  }

  isImage = (e) => {
    var ext = this.getExtension(e.target.files[0].name);
    switch (ext.toLowerCase()) {
      case "jpg":
      case "jpeg":
      case "png":
        //etc
        // return this.onChangeUploadHandler(e);
        return this.onChangeUploadHandler(e)
    }
    return this.onModalError('.jpg , .jpeg , .png Only',e.message);
    
  };

  onChangeUploadHandler(e) {
    let { fields, errors } = this.state;
    fields[e.target.name] = e.target.files[0];
    this.state.errors[e.target.name] = null;
    this.setState({
      fields,
      errors,
    });
  }

  handleInputChange = (e) => {
    const target = e.target;
    const name = target.name;

    if (name == "is_active") {
      this.setState({ is_active: !this.state.is_active });
    }
  };
  render() {

    const { selectOption } = this.state
    return (

      <div>
        <SubHeader
          main_menu_name="จัดการแบนเนอร์"
          main_menu_link=""
          sub_menu_name="รายการแบนเนอร์"
          sub_menu_link="/backoffice/banner"
          sub_menu_function="แก้ไขแบนเนอร์"
        />

        <div class="content">
          <div class="card">

            <div class="card-header header-elements-inline">
              <div className="heading-elements">
                <h4 className="panel-title">แก้ไขแบนเนอร์</h4>

                <div className="card-body">
                  <div className="text-center mb-5">
                    <img
                      className="preview-img"
                      src={this.state.fields["banner_image_show"] || ""}
                      alt={this.state.fields["banner_image_show"] || ""}
                    />
                  </div>
                  <form
                    className="form-horizontal"
                    name="FormCreate"
                    onSubmit={(e) => this.handleSubmit(e)}
                  >
                    <fieldset className="content-group">
                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                          รูปภาพแบนเนอร์{" "}
                        </label>

                        <div className="col-lg-10">
                          <input
                            type="file"
                            className="file-input"
                            data-show-upload="false"
                            autoOrientImage="false"
                            name="banner_image"
                            onChange={this.isImage}
                            accept=".jpg , .png ,.jpeg"
                          />
                          <div className="errorMsg">
                            {this.state.errors.banner_image}
                          </div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                          ลำดับการแสดง
                        </label>
                        <div className="col-lg-4">
                          <input
                            name="banner_sort"
                            type="number"
                            className="form-control"
                            placeholder="ลำดับการแสดง"
                            value = {this.state.fields["banner_sort"] || ""}
                            onChange={this.onHandleCheckChar}
                          />
                          <div className="errorMsg">
                            {this.state.errors.banner_sort}
                          </div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                          สถานะการแสดง
                        </label>
                        <div className="col-lg-10">
                          <label className="switch">
                            <input
                              name="is_active"
                              type="checkbox"
                              onChange={this.handleInputChange}
                              checked={this.state.is_active}
                            />
                            <div className="slider"></div>
                          </label>
                        </div>
                      </div>

                      <div className="form-group row">
                        <div className="col-lg-12 text-right">
                          <input
                            type="submit"
                            className="btn btn-success btn-md btn-create"
                            value="บันทึก"
                          />
                        </div>
                      </div>
                    </fieldset>
                  </form>
                </div>
              
              
              </div>
            </div>
            {this.state.modal}
          </div>
        </div>
      </div>
    );
  }
}
