import React from "react";
import { movieAction } from '../../../_actions'
import SweetAlert from 'react-bootstrap-sweetalert';
import "../movie.css";
import { SubHeader} from "../../../_pagebuilder";
import moment from "moment"
import 'moment/locale/th';
import Select from "react-select";

let modalAlert;

export class Movie_Edit extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      show: false,
      modal: null,
      selectOption: [],
      fields: {},
      errors: {},
      lang: 'th'
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onConfirm = this.onConfirm.bind(this);
    this.onConfirmUpdate = this.onConfirmUpdate.bind(this);
    this.onCancel = this.onCancel.bind(this);
  }

  componentDidMount() {
    let { selectOption } = this.state;

    const {
      match: { params }
    } = this.props;
    this.getMovieByID(params.movie_id);
  }

  getMovieByID(movie_id) {
    
    movieAction.GetMovieByID(movie_id).then(e => {
      if (e.isSuccess === true) {
        let data = e.data[0]
        this.setState({
          fields: {
            ...this.state.fields,
            "movie_id": data.movie_id,
            "movie_code": data.movie_code,
            "movie_link_video": data.movie_link_video,
            "movie_image": data.movie_image,
            "movie_image_show": data.movie_image,
            "movie_date_start": moment(data.movie_date_start,'YYYY-MM-DD , HH:mm:ss').locale('th').format('YYYY-MM-DD'),
            "movie_time_total": data.movie_time_total,
            "movie_name": data.movie_name,
            "movie_genre": data.movie_genre,
            "movie_director": data.movie_director,
            "movie_actors": data.movie_actors,
            "movie_synopsis": data.movie_synopsis,
            "package_option_image_show": data.package_option_image,
            "movie_rating": data.movie_rating,
            "movie_nowshowing_flag": data.movie_nowshowing_flag,
            "movie_advance_booking_flag": data.movie_advance_booking_flag,
            "movie_comingsoon_flag": data.movie_comingsoon_flag,
            "updated_at": moment(data.updated_at,'YYYY-MM-DD , HH:mm:ss').locale('th').format('DD/MM/YYYY HH:mm:ss')
          },
          defaultRating:
          {
            label: data.movie_rating,
            value: data.movie_rating
          },
          "is_nowshowing": data.movie_nowshowing_flag == 'Y' ? true : false ,
          "is_advancebooking": data.movie_advance_booking_flag == 'Y' ? true : false,
          "is_comingsoon": data.movie_comingsoon_flag == 'Y' ? true : false
        });
      } else {
        this.onModalError("Error", "ไม่พบข้อมูลดังกล่าว");
      }
    });
  }

  onModalSuccess(head, body) {
    modalAlert = () => (
      <SweetAlert
          style={{display:'block'}}
          success
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="default"
          title={head}
          onConfirm={this.onConfirm}
          >
          {body}
          </SweetAlert>
      );

      this.setState({ show:true, modal: modalAlert() })
  }

  onModalWarning(head, body) {
    modalAlert = () => (
      <SweetAlert
          style={{display:'block'}}
          warning
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="default"
          title={head}
          onConfirm={this.onCancel}
          >
          {body}
          </SweetAlert>
      );

      this.setState({ show:true, modal: modalAlert() })
  }

  onModalError(head, body) {
    modalAlert = () => (
      <SweetAlert
          style={{display:'block'}}
          error
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="default"
          title={head}
          onConfirm={this.onCancel}
          >
          {body}
          </SweetAlert>
      );

      this.setState({ show:true, modal: modalAlert() })
  }

  onModalQuestion(head, body) {
    modalAlert = () => (
    <SweetAlert
        style={{display:'block'}}
        info
        showCancel
        confirmBtnBsStyle="success"
        cancelBtnBsStyle="default"
        title={head}
        onConfirm={() => this.onConfirmUpdate()}
        onCancel={() => this.onCancel()}
        >
        {body}
        </SweetAlert>
    );

    this.setState({ show:true, modal: modalAlert() })
  }


  handleSubmit(event) {
    event.preventDefault()
    this.setState({ modalOption: null })

    if (this.validateForm()) {
      this.onModalQuestion('Confirm','คุณต้องการแก้ไขข้อมูล ?')
    }
  }

  onConfirm () {
    this.setState({ modal: null })
    window.location.href="/backoffice/movie"
  }

  validateForm() {

    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    if (!fields["movie_code"]) {
      formIsValid = false;
      errors["movie_code"] = "*กรุณากรอกรหัสภาพยนต์";
    }

    if (!fields["movie_name"]) {
      formIsValid = false;
      errors["movie_name"] = "*กรุณากรอกชื่อภาพยนต์";
    }

    if (!fields["movie_image"]) {
      formIsValid = false;
      errors["movie_image"] = "*กรุณาแนบรูปภาพภาพยนต์";
    }

    if (!fields["movie_link_video"]) {
      formIsValid = false;
      errors["movie_link_video"] = "*กรุณากรอกลิงค์วีดีโอ";
    }

    if (!fields["movie_date_start"]) {
      formIsValid = false;
      errors["movie_date_start"] = "*กรุณากรอกวันที่เริ่มฉาย";
    }

    if (!fields["movie_time_total"]) {
      formIsValid = false;
      errors["movie_time_total"] = "*กรุณากรอกจำนวนเวลาฉาย";
    }

    if (!fields["movie_genre"]) {
      formIsValid = false;
      errors["movie_genre"] = "*กรุณากรอก Genres";
    }

    if (!fields["movie_director"]) {
      formIsValid = false;
      errors["movie_director"] = "*กรุณากรอก Director";
    }

    if (!fields["movie_actors"]) {
      formIsValid = false;
      errors["movie_actors"] = "*กรุณากรอก Actor";
    }

    if (!fields["movie_synopsis"]) {
      formIsValid = false;
      errors["movie_synopsis"] = "*กรุณากรอก Synopsis";
    }

    if (!fields["movie_rating"]) {
      formIsValid = false;
      errors["movie_rating"] = "*กรุณาเลือก Rating";
    }

    this.setState({
      errors: errors
    });
    return formIsValid;
  }

  onConfirmUpdate(ev) {
    this.setState({ modal: null })
    let id = this.state.fields['movie_id'];
    
    var formData = new FormData();
    var formData = new FormData();
    formData.append("movie_code",this.state.fields["movie_code"]);
    formData.append("movie_image",this.state.fields["movie_image"]);
    formData.append("movie_link_video", this.state.fields["movie_link_video"]);
    formData.append("movie_date_start", this.state.fields["movie_date_start"]);
    formData.append("movie_time_total",this.state.fields["movie_time_total"]);
    formData.append("movie_name",this.state.fields["movie_name"]);
    formData.append("movie_genre",this.state.fields["movie_genre"]);
    formData.append("movie_director", this.state.fields["movie_director"]);
    formData.append("movie_actors", this.state.fields["movie_actors"]);
    formData.append("movie_synopsis", this.state.fields["movie_synopsis"]);
    formData.append("movie_rating", this.state.fields["movie_rating"]);
    formData.append("movie_nowshowing_flag", this.state.is_nowshowing == true ? 'Y' : 'N');
    formData.append("movie_advance_booking_flag", this.state.is_advancebooking == true ? 'Y' : 'N');
    formData.append("movie_comingsoon_flag", this.state.is_comingsoon == true ? 'Y' : 'N');
    movieAction.UpdateMovie(formData, id).then(e => {
      if(e.isSuccess === true) {
        this.onModalSuccess('Success', e.message)
      } else {
        this.onModalError('Error', e.message)
      }
    })
  }

  onCancel(ev) {
    this.setState({ modal: null });
  }


  onHandleCheckChar = (e) =>{
    if(!e.target.value.match(/^[+ฯะัาำิีึืฺุูเแโใไๅๆ็่้๊๋์,ก-ฮa-zA-Z0-9.:\s#/_-]+$/i) && e.target.value.length>0){
      this.onModalError('ไม่สามารถใช้ตัวอักษรพิเศษได้',e.message)
      e.target.value = e.target.value.toString();
    }else{
      return this.handleChange(e)
    }
  }

  handleChange = (e) => {
    if(!e.target.value.match(/^[+ฯะัาำิีึืฺุูเแโใไๅๆ็่้๊๋์,ก-ฮa-zA-Z0-9.:\s#/_-]+$/i) && e.target.value.length>0){
      this.onModalError('ไม่สามารถใช้ตัวอักษรพิเศษได้',e.message)
      e.target.value = e.target.value.toString();
    }else{
      let { fields, errors } = this.state;
      fields[e.target.name] = e.target.value;
      this.state.errors[e.target.name] = null;
      this.setState({ fields, errors });
    }
  };

  getExtension(filename) {
    var parts = filename.split(".");
    return parts[parts.length - 1];
  }

  isImage = (e) => {
    var ext = this.getExtension(e.target.files[0].name);
    switch (ext.toLowerCase()) {
      case "jpg":
      case "jpeg":
      case "png":
        //etc
        // return this.onChangeUploadHandler(e);
        return this.onChangeUploadHandler(e)
    }
    return this.onModalError('.jpg , .jpeg , .png Only',e.message);
    
  };

  onChangeUploadHandler(e) {
    let { fields, errors } = this.state;
    fields[e.target.name] = e.target.files[0];
    this.state.errors[e.target.name] = null;
    this.setState({
      fields,
      errors,
    });
  }


  handleSelectChange = (e, action) => {
    let { fields, errors } = this.state;
    if (action.name === "movie_rating"){
      this.setState({
        defaultRating:
          {
            label: e.label,
            value: e.value
          }
      })
    }
    fields[action.name] = e.value;
    this.state.errors[action.name] = null;
    this.setState({ fields, errors });
  };


  handleInputChange = (e) => {
    const target = e.target;
    const name = target.name;

    if (name == "is_nowshowing") {
      console.log(!this.state.is_nowshowing);
      this.setState({ is_nowshowing: !this.state.is_nowshowing });
    }

    if (name == "is_advancebooking") {
      this.setState({
        is_advancebooking: !this.state.is_advancebooking,
      });
    }

    if (name == "is_comingsoon") {
      this.setState({
        is_comingsoon: !this.state.is_comingsoon,
      });
    }
  };
  render() {

    const { selectOption } = this.state
    
    var option_rate = [
      { value: 'G', label: 'G' },
      { value: 'PG', label: 'PG' },
      { value: 'PG-13', label: 'PG-13' },
      { value: 'R', label: 'R' },
      { value: 'NC-17', label: 'NC-17' },
    ];

    console.log(this.state);
    return (

      <div>
        <SubHeader 
          main_menu_name="จัดการภาพยนต์" 
          main_menu_link="" 
          sub_menu_name="รายการภาพยนต์" 
          sub_menu_link="/backoffice/movie" 
          sub_menu_function="แก้ไขภาพยนต์" />

        <div class="content">
          <div class="card">

            <div class="card-header header-elements-inline">
              <div className="heading-elements">
                <h4 className="panel-title">แก้ไขภาพยนต์</h4>

                <div className="card-body">
                  <div className="text-center mb-5">
                    <img
                      className="preview-img"
                      src={this.state.fields["movie_image_show"] || ""}
                      alt={this.state.fields["movie_image_show"] || ""}
                    />
                  </div>
                  <form
                    className="form-horizontal"
                    name="FormCreate"
                    onSubmit={(e) => this.handleSubmit(e)}
                  >
                    <fieldset className="content-group">
                    <div className="form-group row">
                        <label className="control-label col-lg-2">
                          รหัสภาพยนต์
                        </label>
                        <div className="col-lg-4">
                          <input
                            name="movie_code"
                            type="text"
                            className="form-control"
                            placeholder="รหัสภาพยนต์"
                            value = {this.state.fields["movie_code"]}
                            onChange={this.onHandleCheckChar}
                          />
                          <div className="errorMsg">
                            {this.state.errors.movie_code}
                          </div>
                        </div>
                      </div>
                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                          รูปภาพภาพยนต์{" "}
                          <div className="text-danger">(390*390 px)</div>
                        </label>

                        <div className="col-lg-10">
                          <input
                            type="file"
                            className="file-input"
                            data-show-upload="false"
                            autoOrientImage="false"
                            name="movie_image"
                            onChange={this.isImage}
                            accept=".jpg , .png ,.jpeg"
                          />
                          <div className="errorMsg">
                            {this.state.errors.movie_image}
                          </div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                          ชื่อภาพยนต์
                        </label>
                        <div className="col-lg-10">
                          <input
                            name="movie_name"
                            type="text"
                            className="form-control"
                            placeholder="ชื่อภาพยนต์"
                            value = {this.state.fields["movie_name"] || ""}
                            onChange={this.onHandleCheckChar}
                            
                          />
                          <div className="errorMsg">
                            {this.state.errors.movie_name}
                          </div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                          ลิงค์วีดีโอ Youtube
                        </label>
                        <div className="col-lg-10">
                          <input
                            name="movie_link_video"
                            type="text"
                            className="form-control"
                            placeholder="ลิงค์แพ็กเกจ"
                            value = {this.state.fields["movie_link_video"] || ""}
                            onChange={this.handleChange}
                          />
                          <div className="text-secondary">ค่าที่อยู่หลัง v= เช่น https://www.youtube.com/watch?v=JM1U-Whb-P0 ให้ใส่ "JM1U-Whb-P0"</div>
                          <div className="errorMsg">
                            {this.state.errors.movie_link_video}
                          </div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                          วันที่เริ่มฉาย
                        </label>
                        <div className="col-lg-4">
                          <input
                            name="movie_date_start"
                            type="date"
                            className="form-control"
                            placeholder="วันที่เริ่มฉาย"
                            value={this.state.fields["movie_date_start"]}
                            onChange={this.handleChange}
                          />
                          <div className="errorMsg">
                            {this.state.errors.movie_date_start}
                          </div>
                        </div>

                        <label className="control-label col-lg-2">
                          จำนวนเวลาฉาย (ชั่วโมง:นาที)
                        </label>
                        <div className="col-lg-3">
                          <input
                            name="movie_time_total"
                            type="time"
                            className="form-control"
                            placeholder="จำนวนเวลาฉาย"
                            value={this.state.fields["movie_time_total"]}
                            onChange={this.handleChange}
                          />
                          <div className="errorMsg">
                            {this.state.errors.movie_time_total}
                          </div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                          Genres
                        </label>
                        <div className="col-lg-4">
                          <input
                            name="movie_genre"
                            type="text"
                            className="form-control"
                            placeholder="Genres"
                            value = {this.state.fields["movie_genre"] || ""}
                            onChange={this.handleChange}
                          />
                          <div className="errorMsg">
                            {this.state.errors.movie_genre}
                          </div>
                        </div>

                        <label className="control-label col-lg-2">
                          Director
                        </label>
                        <div className="col-lg-4">
                          <input
                            name="movie_director"
                            type="text"
                            className="form-control"
                            placeholder="Director"
                            value = {this.state.fields["movie_director"] || ""}
                            onChange={this.handleChange}
                          />
                          <div className="errorMsg">
                            {this.state.errors.movie_director}
                          </div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                          Actor
                        </label>
                        <div className="col-lg-10">
                          <input
                            name="movie_actors"
                            type="text"
                            className="form-control"
                            placeholder="Actor"
                            value = {this.state.fields["movie_actors"] || ""}
                            onChange={this.handleChange}
                          />
                          <div className="errorMsg">
                            {this.state.errors.movie_actors}
                          </div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                          Synopsis
                        </label>
                        <div className="col-lg-10">
                          <textarea 
                            name="movie_synopsis" 
                            className="form-control" 
                            rows="3"
                            value={this.state.fields["movie_synopsis"]}
                            onChange={this.handleChange}
                          />
                          <div className="errorMsg">
                            {this.state.errors.movie_actors}
                          </div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                          Rate
                        </label>
                        <div className="col-lg-4">
                          <Select
                          name="movie_rating"
                          placeholder="เลือก rating"
                          onChange={this.handleSelectChange}
                          options={option_rate}
                          className="Select-menu-outer"
                          value={this.state.defaultRating}
                        />
                        <div className="errorMsg">
                            {this.state.errors.movie_rating}
                          </div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                          สถานะ Now Showing
                        </label>
                        <div className="col-lg-10">
                          <label className="switch">
                            <input
                              name="is_nowshowing"
                              type="checkbox"
                              onChange={this.handleInputChange}
                              checked={this.state.is_nowshowing}
                            />
                            <div className="slider"></div>
                          </label>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                          สถานะ Advance Booking
                        </label>
                        <div className="col-lg-10">
                          <label className="switch">
                            <input
                              name="is_advancebooking"
                              type="checkbox"
                              onChange={this.handleInputChange}
                              checked={this.state.is_advancebooking}
                            />
                            <div className="slider"></div>
                          </label>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                          สถานะ Coming Soon
                        </label>
                        <div className="col-lg-10">
                          <label className="switch">
                            <input
                              name="is_comingsoon"
                              type="checkbox"
                              onChange={this.handleInputChange}
                              checked={this.state.is_comingsoon}
                            />
                            <div className="slider"></div>
                          </label>
                        </div>
                      </div>

                      <div className="form-group row">
                        <div className="col-lg-12 text-right">
                          <input
                            type="submit"
                            className="btn btn-success btn-md btn-create"
                            value="บันทึก"
                          />
                        </div>
                      </div>
                    </fieldset>
                  </form>
                </div>
              
              
              </div>
            </div>
            {this.state.modal}
          </div>
        </div>
      </div>
    );
  }
}
