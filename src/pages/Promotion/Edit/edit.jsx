import React from "react";
import { promotionAction } from '../../../_actions'
import SweetAlert from 'react-bootstrap-sweetalert';
import "../promotion.css";
import { SubHeader} from "../../../_pagebuilder";
import moment from "moment"
import 'moment/locale/th';
import Select from "react-select";

let modalAlert;

export class Promotion_Edit extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      show: false,
      modal: null,
      selectOption: [],
      fields: {},
      errors: {},
      lang: 'th'
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onConfirm = this.onConfirm.bind(this);
    this.onConfirmUpdate = this.onConfirmUpdate.bind(this);
    this.onCancel = this.onCancel.bind(this);
  }

  componentDidMount() {
    let { selectOption } = this.state;

    const {
      match: { params }
    } = this.props;

    this.getPromotionByID(params.promotion_id);
  }

  getPromotionByID(promotion_id) {
    promotionAction.GetPromotionByID(promotion_id).then(e => {
      if (e.isSuccess === true) {
        let data = e.data[0]
        this.setState({
          fields: {
            ...this.state.fields,
            "promotion_id": data.promotion_id,
            "promotion_image": data.promotion_image,
            "promotion_image_show": data.promotion_image,
            "promotion_name_th": data.promotion_name_th,
            "promotion_name_en": data.promotion_name_en,
            "promotion_desc_th": data.promotion_desc_th,
            "promotion_desc_en": data.promotion_desc_en,
            "promotion_start_date": moment(data.promotion_start_date,'YYYY-MM-DD , HH:mm:ss').locale('th').format('YYYY-MM-DD'),
            "promotion_stop_date": moment(data.promotion_stop_date,'YYYY-MM-DD , HH:mm:ss').locale('th').format('YYYY-MM-DD'),
            "promotion_show_date": moment(data.promotion_show_date,'YYYY-MM-DD , HH:mm:ss').locale('th').format('YYYY-MM-DD'),
            "is_active": data.is_active,
            "updated_at": moment(data.updated_at,'YYYY-MM-DD , HH:mm:ss').locale('th').format('DD/MM/YYYY HH:mm:ss')
          },
          "is_active": data.is_active,
        });
      
      } else {
        this.onModalError("Error", "ไม่พบข้อมูลดังกล่าว");
      }
    });
  }

  onModalSuccess(head, body) {
    modalAlert = () => (
      <SweetAlert
          style={{display:'block'}}
          success
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="default"
          title={head}
          onConfirm={this.onConfirm}
          >
          {body}
          </SweetAlert>
      );

      this.setState({ show:true, modal: modalAlert() })
  }

  onModalWarning(head, body) {
    modalAlert = () => (
      <SweetAlert
          style={{display:'block'}}
          warning
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="default"
          title={head}
          onConfirm={this.onCancel}
          >
          {body}
          </SweetAlert>
      );

      this.setState({ show:true, modal: modalAlert() })
  }

  onModalError(head, body) {
    modalAlert = () => (
      <SweetAlert
          style={{display:'block'}}
          error
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="default"
          title={head}
          onConfirm={this.onCancel}
          >
          {body}
          </SweetAlert>
      );

      this.setState({ show:true, modal: modalAlert() })
  }

  onModalQuestion(head, body) {
    modalAlert = () => (
    <SweetAlert
        style={{display:'block'}}
        info
        showCancel
        confirmBtnBsStyle="success"
        cancelBtnBsStyle="default"
        title={head}
        onConfirm={() => this.onConfirmUpdate()}
        onCancel={() => this.onCancel()}
        >
        {body}
        </SweetAlert>
    );

    this.setState({ show:true, modal: modalAlert() })
  }


  handleSubmit(event) {
    event.preventDefault()
    this.setState({ modalOption: null })

    if (this.validateForm()) {
      this.onModalQuestion('Confirm','คุณต้องการแก้ไขข้อมูล ?')
    }
  }

  onConfirm () {
    this.setState({ modal: null })
    window.location.href="/backoffice/promotion"
  }

  validateForm() {

    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    if (!fields["promotion_image"]) {
      formIsValid = false;
      errors["promotion_image"] = "*กรุณาแนบรูปโปรโมชัน";
    }

    if (!fields["promotion_name_th"]) {
      formIsValid = false;
      errors["promotion_name_th"] = "*กรุณากรอกหัวข้อโปรโมชัน (TH)";
    }

    if (!fields["promotion_name_en"]) {
      formIsValid = false;
      errors["promotion_name_en"] = "*กรุณากรอกหัวข้อโปรโมชัน (EN)";
    }

    if (!fields["promotion_desc_th"]) {
      formIsValid = false;
      errors["promotion_desc_th"] = "*กรุณากรอกรายละเอียดโปรโมชัน (TH)";
    }

    if (!fields["promotion_desc_en"]) {
      formIsValid = false;
      errors["promotion_desc_en"] = "*กรุณากรอกรายละเอียดโปรโมชัน (EN)";
    }

    if (!fields["promotion_start_date"]) {
      formIsValid = false;
      errors["promotion_start_date"] = "*กรุณากรอกวันที่เริ่มโปรโมชัน";
    }

    if (!fields["promotion_stop_date"]) {
      formIsValid = false;
      errors["promotion_stop_date"] = "*กรุณากรอกวันที่สิ้นสุดโปรโมชัน";
    }

    if (!fields["promotion_show_date"]) {
      formIsValid = false;
      errors["promotion_show_date"] = "*กรุณากรอกวันที่โชว์โปรโมชัน";
    }

    this.setState({
      errors: errors
    });
    return formIsValid;
  }

  onConfirmUpdate(ev) {
    this.setState({ modal: null })
    let id = this.state.fields['promotion_id'];
    
    var formData = new FormData();
    formData.append("promotion_image",this.state.fields["promotion_image"]);
    formData.append("promotion_name_th",this.state.fields["promotion_name_th"]);
    formData.append("promotion_name_en",this.state.fields["promotion_name_en"]);
    formData.append("promotion_desc_th",this.state.fields["promotion_desc_th"]);
    formData.append("promotion_desc_en",this.state.fields["promotion_desc_en"]);
    formData.append("promotion_start_date",this.state.fields["promotion_start_date"]);
    formData.append("promotion_stop_date",this.state.fields["promotion_stop_date"]);
    formData.append("promotion_show_date",this.state.fields["promotion_show_date"]);
    formData.append("is_active", this.state.is_active == true ? 1 : 0);

    promotionAction.UpdatePromotion(formData, id).then(e => {
      if(e.isSuccess === true) {
        this.onModalSuccess('Success', e.message)
      } else {
        this.onModalError('Error', e.message)
      }
    })
  }

  onCancel(ev) {
    this.setState({ modal: null });
  }

  onHandleCheckChar = (e) =>{
    if(!e.target.value.match(/^[+ฯะัาำิีึืฺุูเแโใไๅๆ็่้๊๋์,ก-ฮa-zA-Z0-9.:\s#/_-]+$/i) && e.target.value.length>0){
      this.onModalError('ไม่สามารถใช้ตัวอักษรพิเศษได้',e.message)
      e.target.value = e.target.value.toString();
    }else{
      return this.handleChange(e)
    }
  }

  handleChange = (e) => {
    if(!e.target.value.match(/^[+ฯะัาำิีึืฺุูเแโใไๅๆ็่้๊๋์,ก-ฮa-zA-Z0-9.:\s#/_-]+$/i) && e.target.value.length>0){
      this.onModalError('ไม่สามารถใช้ตัวอักษรพิเศษได้',e.message)
      e.target.value = e.target.value.toString();
    }else{
      let { fields, errors } = this.state;
      fields[e.target.name] = e.target.value;
      this.state.errors[e.target.name] = null;
      this.setState({ fields, errors });
    }
  };

  isImage = (e) => {
    console.log(e.target);
    var ext = this.getExtension(e.target.files[0].name);
    switch (ext.toLowerCase()) {
      case "jpg":
      case "jpeg":
      case "png":
        //etc
        // return this.onChangeUploadHandler(e);
        return this.onChangeUploadHandler(e)
    }
    return this.onModalError('.jpg , .jpeg , .png Only',e.message);
    
  };

  getExtension(filename) {
    var parts = filename.split(".");
    return parts[parts.length - 1];
  }

  onChangeUploadHandler(e) {
    let { fields, errors } = this.state;
    fields[e.target.name] = e.target.files[0];
    this.state.errors[e.target.name] = null;
    this.setState({
      fields,
      errors,
    });
  }

  handleInputChange = (e) => {
    const target = e.target;
    const name = target.name;

    if (name == "is_active") {
      this.setState({ is_active: !this.state.is_active });
    }
  };
  render() {

    const { selectOption } = this.state
    return (

      <div>
        <SubHeader
          main_menu_name="จัดการโปรโมชัน"
          main_menu_link=""
          sub_menu_name="รายการโปรโมชัน"
          sub_menu_link="/backoffice/promotion"
          sub_menu_function="แก้ไขโปรโมชัน"
        />

        <div class="content">
          <div class="card">

            <div class="card-header header-elements-inline">
              <div className="heading-elements">
                <h4 className="panel-title">แก้ไขโปรโมชัน</h4>

                <div className="card-body">
                  <div className="text-center mb-5">
                    <img
                      className="preview-img"
                      src={this.state.fields["promotion_image_show"] || ""}
                      alt={this.state.fields["promotion_image_show"] || ""}
                    />
                  </div>
                  <form
                    className="form-horizontal"
                    name="FormCreate"
                    onSubmit={(e) => this.handleSubmit(e)}
                  >
                    <fieldset className="content-group">
                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                          รูปภาพโปรโมชัน{" "}
                        </label>

                        <div className="col-lg-10">
                          <input
                            type="file"
                            className="file-input"
                            data-show-upload="false"
                            autoOrientImage="false"
                            name="promotion_image"
                            onChange={this.isImage}
                            accept=".jpg , .png ,.jpeg"
                          />
                          <div className="errorMsg">
                            {this.state.errors.promotion_image}
                          </div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                          หัวข้อโปรโมชัน (TH)
                        </label>
                        <div className="col-lg-10">
                          <input
                            name="promotion_name_th"
                            type="text"
                            className="form-control"
                            placeholder="หัวข้อโปรโมชัน (TH)"
                            value = {this.state.fields["promotion_name_th"] || ""}
                            onChange={this.onHandleCheckChar}
                          />
                          <div className="errorMsg">
                            {this.state.errors.promotion_name_th}
                          </div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                          หัวข้อโปรโมชัน (EN)
                        </label>
                        <div className="col-lg-10">
                          <input
                            name="promotion_name_en"
                            type="text"
                            className="form-control"
                            placeholder="หัวข้อโปรโมชัน (EN)"
                            value = {this.state.fields["promotion_name_en"] || ""}
                            onChange={this.onHandleCheckChar}
                          />
                          <div className="errorMsg">
                            {this.state.errors.promotion_name_en}
                          </div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                           รายละเอียดโปรโมชัน (TH)
                        </label>
                        <div className="col-lg-10">
                          <textarea 
                            name="promotion_desc_th" 
                            className="form-control"
                            value = {this.state.fields["promotion_desc_th"] || ""}
                            rows="5"
                            onChange={this.onHandleCheckChar} 
                            />
                          <div className="errorMsg">
                            {this.state.errors.promotion_desc_th}
                          </div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                           รายละเอียดโปรโมชัน (EN)
                        </label>
                        <div className="col-lg-10">
                          <textarea 
                            name="promotion_desc_en" 
                            className="form-control"
                            value = {this.state.fields["promotion_desc_en"] || ""}
                            rows="5"
                            onChange={this.onHandleCheckChar} 
                            />
                          <div className="errorMsg">
                            {this.state.errors.promotion_desc_en}
                          </div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                          วันที่เริ่มโปรโมชัน
                        </label>
                        <div className="col-lg-4">
                          <input
                            name="promotion_start_date"
                            type="date"
                            className="form-control"
                            placeholder="วันที่เริ่มโปรโมชัน"
                            value = {this.state.fields["promotion_start_date"] || ""}
                            onChange={this.onHandleCheckChar}
                          />
                          <div className="errorMsg">
                            {this.state.errors.promotion_start_date}
                          </div>
                        </div>

                        <label className="control-label col-lg-2">
                          วันที่สิ้นสุดโปรโมชัน
                        </label>
                        <div className="col-lg-4">
                          <input
                            name="promotion_stop_date"
                            type="date"
                            className="form-control"
                            placeholder="วันที่สิ้นสุดโปรโมชัน"
                            value = {this.state.fields["promotion_stop_date"] || ""}
                            onChange={this.onHandleCheckChar}
                          />
                          <div className="errorMsg">
                            {this.state.errors.promotion_stop_date}
                          </div>
                        </div>
                      </div>


                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                          วันที่โชว์โปรโมชัน
                        </label>
                        <div className="col-lg-4">
                          <input
                            name="promotion_show_date"
                            type="date"
                            className="form-control"
                            placeholder="วันที่โชว์โปรโมชัน"
                            value = {this.state.fields["promotion_show_date"] || ""}
                            onChange={this.onHandleCheckChar}
                          />
                          <div className="errorMsg">
                            {this.state.errors.promotion_show_date}
                          </div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                          สถานะการแสดง
                        </label>
                        <div className="col-lg-10">
                          <label className="switch">
                            <input
                              name="is_active"
                              type="checkbox"
                              onChange={this.handleInputChange}
                              checked={this.state.is_active}
                            />
                            <div className="slider"></div>
                          </label>
                        </div>
                      </div>

                      <div className="form-group row">
                        <div className="col-lg-12 text-right">
                          <input
                            type="submit"
                            className="btn btn-success btn-md btn-create"
                            value="บันทึก"
                          />
                        </div>
                      </div>
                    </fieldset>
                  </form>
                </div>
              
              
              </div>
            </div>
            {this.state.modal}
          </div>
        </div>
      </div>
    );
  }
}
