import React from "react";
import $ from "jquery";
import { promotionAction } from "../../../_actions";
import { SubHeader} from "../../../_pagebuilder";
import { Base_API } from "../../../_constants/matcher";
import moment from "moment"
import 'moment/locale/th';

import SweetAlert from "react-bootstrap-sweetalert";
import "../promotion.css";

$.DataTable = require("datatables.net");
let modalAlert;

export class Promotion_List extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.openModalHandler = this.openModalHandler.bind(this);
    this.closeModalHandler = this.closeModalHandler.bind(this);

    this.state = {
      show: false,
      modal: null,
      isModal: false,
      lang: 'th'
    };
  }

  openModalHandler() {
    this.setState({
      isModal: true
    });
  }

  closeModalHandler = () => {
    this.setState({
      isModal: false
    });
  };

  componentDidMount() {
    const columns = [
      {
        title: "รูปภาพ",
        class: "text-center",
        data: "promotion_image"
      },
      {
        title: "หัวข้อโปรโมชัน (TH)",
        class: "text-center",
        data: "promotion_name_th"
      },
      {
        title: "วันที่เริ่มโปรโมชัน",
        class: "text-center",
        data: "promotion_start_date"
      },
      {
        title: "วันที่สิ้นสุดโปรโมชัน",
        class: "text-center",
        data: "promotion_stop_date"
      },
      {
        title: "วันที่ต้องการโชว์โปรโมชัน",
        class: "text-center",
        data: "promotion_show_date"
      },
      {
        title: "สถานะการแสดง",
        class: "text-center",
        data: "is_active"
      },
      {
        title: "วันที่แก้ไข",
        class: "text-center",
        data: "updated_at"
      },
      {
        title: "จัดการ",
        class: "text-center",
        data: "promotion_id"
      }
    ];
    let table = $(this.refs.main).DataTable({
      columnDefs: [
        {
          targets: 0,
          createdCell: function(td, cellData, rowData, row, col) {
            $(td)
              .html(`<img class='thumbnail' src='${cellData}' />`);
          },
          orderable: false
        },
        {
          targets: 2,
          createdCell: function(td, cellData, rowData, row, col) {
            $(td).html(moment(cellData,'YYYY-MM-DD , HH:mm:ss').locale('th').format('DD/MM/YYYY'));
          },
          orderable: true
        },
        {
          targets: 3,
          createdCell: function(td, cellData, rowData, row, col) {
            $(td).html(moment(cellData,'YYYY-MM-DD , HH:mm:ss').locale('th').format('DD/MM/YYYY'));
          },
          orderable: true
        },
        {
          targets: 4,
          createdCell: function(td, cellData, rowData, row, col) {
            $(td).html(moment(cellData,'YYYY-MM-DD , HH:mm:ss').locale('th').format('DD/MM/YYYY'));
          },
          orderable: true
        },
        {
          targets: 5,
          createdCell: function(td, cellData, rowData, row, col) {
            $(td)
              .html(`
                <label class="switch">
                  <input name="is_active" class="activeBtn" data-id="${rowData.promotion_id}" type="checkbox" ${cellData==1 ? "checked" : ""}/>
                  <div class="slider"></div>
                </label>
              `);
          },
          orderable: false
        },
        {
          targets: 7,
          createdCell: function(td, cellData, rowData, row, col) {
            let button = `<div class="list-icons">
            <div class="dropdown">
              <a href="#" class="list-icons-item" data-toggle="dropdown">
                <i class="icon-menu9"></i>
              </a>

              <div class="dropdown-menu dropdown-menu-right">
                <a href="/backoffice/promotion/edit/${cellData}" class="dropdown-item">
                  <i class="icon-pencil3 mr-3 mb-1"></i> แก้ไข
                </a>
                <button type='button' class="dropdown-item" name='delete_btn' data-content='${cellData}'>
                  <i class="icon-trash mr-3 mb-1"></i>ลบ
                </button>
              </div>
            </div>
          </div>
          `
            $(td).html(button);
          },
          orderable: false
        }
      ],
      ajax: {
        url: `${Base_API.url_api}/backend/ListPromotion`,
        type: "GET",
        dataType: "JSON"
      },
      order: [[6, "DESC"]],
      columns,
      serverSide: true,
      ordering: true,
      searching: true,
      processing: true,
      bLengthChange: false,
      "language": {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span> '
      },
    });

    var state = this;
    

    $(this.refs.main).on("click", ".activeBtn", function () {
      var id = $(this).data("id");
      var fval = $(this).is(":checked") === true ? 1 : 0;

      var formData = new FormData();
      formData.append("is_active", fval);

      promotionAction.UpdatePromotion(formData, id).then(e => {
        if (e.isSuccess === false) {
          onModalError("Error", e.message);
        } else {
          onModalSuccess("Success", e.message);
        }
      });
    });

    $(this.refs.main).on("click", 'button[name="delete_btn"]', function() {
      var id = $(this).data("content");
      modalAlert = () => (
        <SweetAlert
          style={{ display: "block" }}
          info
          showCancel
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="danger"
          title="Delete"
          onConfirm={() => onConfirmDelete(id)}
          onCancel={() => onConfirm()}
        >
          คุณต้องการลบข้อมูลนี้หรือไม่ ?
        </SweetAlert>
      );

      state.setState({ show: true, modal: modalAlert() });
    });

    function onConfirmDelete(id) {   
      promotionAction.DeletePromotion(id).then(e => {
        if (e.isSuccess === false) {
          onModalError("Error", e.message);
        } else {
          onModalSuccess("Success", e.message);
          table.ajax.reload();
        }
      });
    }

    function onModalSuccess(head, body) {
      modalAlert = () => (
        <SweetAlert
          style={{ display: "block" }}
          success
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="default"
          title={head}
          onConfirm={onConfirm}
        >
          {body}
        </SweetAlert>
      );

      state.setState({ show: true, modal: modalAlert() });
    }

    function onModalError(head, body) {
      modalAlert = () => (
        <SweetAlert
          style={{ display: "block" }}
          error
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="default"
          title={head}
          onConfirm={onConfirm}
        >
          {body}
        </SweetAlert>
      );

      state.setState({ show: true, modal: modalAlert() });
    }

    function onConfirm(ev) {
      state.setState({ modal: null });
    }
  }



  render() {
    return (
      <div>
        <SubHeader 
          main_menu_name="จัดการโปรโมชัน" 
          main_menu_link="" 
          sub_menu_name="รายการโปรโมชัน" 
          sub_menu_link="" 
          sub_menu_function="" />

        <div class="content">
          <div class="card">
            <div class="card-header header-elements-inline text-right">
              <div className="heading-elements">
                <a href={"/backoffice/promotion/add"}>
                  <button
                    type="button"
                    name="btn_add"
                    data-content=""
                    className="btn btn-black-embassy"
                  >
                    เพิ่ม
                  </button>
                </a>
              </div>
            </div>
            <div class="card-body">
              <div className="table-responsive">
                <table id="dataTable" className="table table-hover" ref="main" />
              </div>
              {this.state.modal}
            </div>
          </div>
        </div>
        
      </div>
    );
  }
}
