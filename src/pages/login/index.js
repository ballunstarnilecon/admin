import React from "react";
import { adminAction } from '../../_actions'
import SweetAlert from "react-bootstrap-sweetalert";
import { Redirect } from 'react-router'
import './login.css';

let modalAlert;

class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      submitted: false

    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onCancel = this.onCancel.bind(this);

  }

  componentDidMount() {
    document.body.style.backgroundColor = "#1f2022";
    document.body.style.backgroundImage = `url(${process.env.PUBLIC_URL}/images/bg-page.jpeg)`;
    document.body.style.backgroundRepeat = 'repeat-x';
    document.body.style.backgroundPosition = 'center top';
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }  

  handleSubmit(e) {
    e.preventDefault();
    this.setState({ submitted: true });
    this.handleLogin(this.state.username,this.state.password)
  }

  handleLogin(username,password) {
    let inputData = {
      username : this.state.username,
      password : this.state.password
    }
    
    adminAction.checkLogin(inputData).then(e => {   
      let loginState = e.data
      if (loginState.isSuccess) {
        localStorage.setItem("username-embassy", loginState.data.username)
        localStorage.setItem("login-embassy", "true")
        localStorage.setItem("login-backoffice-embassy", "true")
        localStorage.setItem("role-embassy", loginState.data.manager_role)
        sessionStorage.setItem("active", "ManageMovie");
        window.location.href = "/backoffice/movie";

      } else {
        // console.log(e);
        // this.onModalError("Login fail", e.message);
        this.onModalError("Error", loginState.message);
      }
    });

    // if (this.state.username === "admin" && this.state.password === "pass") {
    //   localStorage.setItem("username-embassy", "admin")
    //   localStorage.setItem("login-embassy", "true")
    //   localStorage.setItem("login-backoffice-embassy", "true")
    //   window.location.href = "/backoffice/homepage";
    // } else {
    //   this.onModalError("Error", "Username หรือ Password ผิด กรุณาตรวจสอบใหม่อีกครั้ง");
    // }
  }

  onModalError(head, body) {
    modalAlert = () => (
      <SweetAlert
        style={{ display: "block" }}
        error
        confirmBtnBsStyle="success"
        cancelBtnBsStyle="default"
        title={head}
        onConfirm={this.onCancel}
      >
        {body}
      </SweetAlert>
    );

    this.setState({ show: true, modal: modalAlert() });
  }

  onCancel(ev) {
    this.setState({ modal: null });
  }
  
  
  render() {
    const loginStatus = localStorage.getItem('login-3bb');
    const { username, password, submitted } = this.state;

    return (
      <div className="page-content">

        <div className="content-wrapper">

          <div className="content d-flex justify-content-center align-items-center">

            <form className="login-form">
              <div className="card mb-0">
                <div className="card-body">
                  <div className="text-center mb-3">
                    <img src={process.env.PUBLIC_URL+'/images/logo_mobile_embassy.svg'} alt="" className="logo-login"></img>
                    <div className="mt-2">ระบบจัดการหลังบ้าน Embassy</div>
                  </div>

                  <div className="form-group form-group-feedback form-group-feedback-left">
                    <input 
                      type="text" 
                      className="form-control" 
                      placeholder="Username" 
                      name="username"
                      value={username}
                      onChange={this.handleChange}
                    />
                    <div className="form-control-feedback">
                      <i className="icon-user text-muted"></i>
                    </div>
                  </div>

                  <div className="form-group form-group-feedback form-group-feedback-left">
                    <input 
                      type="password" 
                      className="form-control" 
                      placeholder="Password"
                      name="password"
                      value={password}
                      onChange={this.handleChange}
                    />
                    <div className="form-control-feedback">
                      <i className="icon-lock2 text-muted"></i>
                    </div>
                  </div>

                  <div className="form-group">
                    <button 
                      type="button" 
                      className="btn btn-black-embassy btn-block"
                      value={submitted}
                      onClick={this.handleSubmit}
                    >
                        เข้าสู่ระบบ
                        <i className="icon-circle-right2 ml-2"></i>
                      </button>
                  </div>
                </div>
              </div>
            </form>
            {this.state.modal}
          </div>



        </div>

      </div>
    );
  }
}
  
export default LoginPage;