import React from "react";
import { homeAction } from '../../../../_actions'
import SweetAlert from 'react-bootstrap-sweetalert';
import "../../home.css";
import { SubHeader } from "../../../../_pagebuilder";

// var moment = require('moment'); // require
import moment from "moment"
import 'moment/locale/th';

let modalAlert;

export class Banner_Sub_Home_Edit extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      show: false,
      modal: null,
      modalOption: {},
      fields: {},
      errors: {},
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onConfirm = this.onConfirm.bind(this);
    this.onConfirmUpdate = this.onConfirmUpdate.bind(this);
    this.onCancel = this.onCancel.bind(this);
  }

  componentDidMount() {
    const {
      match: { params }
    } = this.props;
    this.getBannerSubHomeDataByID(params.banner_sub_home_id);
  }

  getBannerSubHomeDataByID(banner_sub_home_id) {
    homeAction.getBannerSub(banner_sub_home_id).then(e => {
      console.log(e);
      if (e.isSuccess === true) {
        this.setState({
          fields: {
            ...this.state.fields,
            "banner_sub_home_id": e.data.banner_sub_home_id,
            "banner_sub_home_link": e.data.banner_sub_home_link,
            "banner_sub_home_image": e.data.banner_sub_home_image,
            "banner_sub_home_image_show": e.data.banner_sub_home_image,
            "updated_at": moment(e.data.updated_at,'DD-MM-YYYY , HH:mm:ss').locale('th').format('DD/MM/YYYY HH:mm:ss'),
            "isActive": e.data.is_active

          }
        });

      } else {
        this.onModalError("Error", "ไม่พบข้อมูลดังกล่าว");
      }
    });
  }

  onModalSuccess(head, body) {
    modalAlert = () => (
      <SweetAlert
          style={{display:'block'}}
          success
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="default"
          title={head}
          onConfirm={this.onConfirm}
          >
          {body}
          </SweetAlert>
      );

      this.setState({ show:true, modal: modalAlert() })
  }

  onModalWarning(head, body) {
    modalAlert = () => (
      <SweetAlert
          style={{display:'block'}}
          warning
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="default"
          title={head}
          onConfirm={this.onCancel}
          >
          {body}
          </SweetAlert>
      );

      this.setState({ show:true, modal: modalAlert() })
  }

  onModalError(head, body) {
    modalAlert = () => (
      <SweetAlert
          style={{display:'block'}}
          error
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="default"
          title={head}
          onConfirm={this.onCancel}
          >
          {body}
          </SweetAlert>
      );

      this.setState({ show:true, modal: modalAlert() })
  }

  onModalQuestion(head, body) {
    modalAlert = () => (
    <SweetAlert
        style={{display:'block'}}
        info
        showCancel
        confirmBtnBsStyle="success"
        cancelBtnBsStyle="default"
        title={head}
        onConfirm={() => this.onConfirmUpdate()}
        onCancel={() => this.onCancel()}
        >
        {body}
        </SweetAlert>
    );

    this.setState({ show:true, modal: modalAlert() })
  }


  handleSubmit(event) {
    event.preventDefault()
    this.setState({ modalOption: null })

    if (this.validateForm()) {
      this.onModalQuestion('Confirm','คุณต้องการแก้ไขข้อมูล ?')
    }
  }

  onConfirm () {
    this.setState({ modal: null })
    window.location.href="/backoffice/manage-home/banner-sub/edit/1"
  }

  validateForm() {

    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    if (!fields["banner_sub_home_image"]) {
      formIsValid = false;
      errors["banner_sub_home_image"] = "*กรุณาแนบรูป";
    }

    if (!fields["banner_sub_home_link"]) {
      formIsValid = false;
      errors["banner_sub_home_link"] = "*กรุณากรอกลิงค์รูป";
    }

    this.setState({
      errors: errors
    });
    return formIsValid;
  }

  onConfirmUpdate(ev) {
    this.setState({ modal: null })
    let id = this.state.fields['banner_sub_home_id'];
    
    var formData = new FormData();
    formData.append("banner_sub_home_image", this.state.fields["banner_sub_home_image"]);
    formData.append("banner_sub_home_link", this.state.fields["banner_sub_home_link"]);
    formData.append("is_active", this.state.fields['isActive']);

    homeAction.updateBannerSub(formData,id).then(e => {
      if(e.isSuccess === true) {
        this.onModalSuccess('Success', e.message)
      } else {
        this.onModalError('Error', e.message)
      }
    })
  }

  onCancel(ev) {
    this.setState({ modal: null });
  }

  handleChange = (e) => {
      let { fields, errors } = this.state;
      fields[e.target.name] = e.target.value;
      this.state.errors[e.target.name] = null;
      this.setState({ fields, errors });
  };

  onChangeUploadHandler = e => {
    let { fields, errors } = this.state;
    fields[e.target.name] = e.target.files[0];
    this.state.errors[e.target.name] = null;
    this.setState({
      fields,
      errors
    });
  };

  handleSelectChange = (e, action) => {
    let { fields, errors } = this.state;
    fields[action.name] = e.value;
    this.state.errors[action.name] = null;
    this.setState({ fields, errors });

  };


handleInputChange = e => {

  const target = e.target;
  const name = target.name;
  let { fields, errors } = this.state;
  this.setState({ fields });
}


  render() {
    return (

      <div>
        <SubHeader 
          main_menu="จัดการหน้า Home" 
          sub_menu="จัดการรูปรับสิทธิพิเศษ" 
          sub_menu_link="" 
          sub_menu_function="แก้ไขรูปรับสิทธิพิเศษ" />

        <div class="content">
          <div class="card">

            <div class="card-header header-elements-inline">
              <div className="heading-elements">
                <h4 className="panel-title">แก้ไขรูปรับสิทธิพิเศษ</h4>

                <div class="card-body">

                  <form className="form-horizontal" onSubmit={(e) => this.handleSubmit(e)}>
                    <fieldset className="content-group">
                      <div className="text-center mb-5">
                          <img
                            className="preview-img"
                            src={this.state.fields["banner_sub_home_image_show"] || ""}
                          />
                        </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">รูปภาพ
                          <br /><span className="text-danger"> (1680*410 px)</span>
                        </label>
                        
                        <div className="col-lg-10">
                          <input 
                            type="file" 
                            className="file-input" 
                            data-show-upload="false" 
                            autoOrientImage="false"
                            name="banner_sub_home_image" 
                            
                            onChange={this.onChangeUploadHandler}
                            />
                          <div className="errorMsg">{this.state.errors.banner_sub_home_image}</div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">ลิงค์รูปภาพ</label>
                        <div className="col-lg-10">
                          <input
                              name="banner_sub_home_link"
                              type="text"
                              className="form-control"
                              placeholder="ลิงค์รูปภาพ"
                              value={this.state.fields["banner_sub_home_link"] || ""}
                              onChange={this.handleChange}
                            />
                            <div className="errorMsg">{this.state.errors.banner_sub_home_link}</div>
                        </div>
                      </div>
                    
                      <div className="form-group row">
                        <label className="control-label col-lg-2">วันที่อัพเดต</label>
                        <div className="col-lg-10">
                          <span>{this.state.fields.updated_at}</span>
                        </div>
                      </div>
                    

                      <div className="form-group row">
                        <div className="col-lg-12 text-right">
                          <input
                            type="submit"
                            className="btn btn-success btn-md btn-update"
                            value="บันทึก"
                          />
                        </div>
                      </div>
                    
                    </fieldset>
                  </form>
                </div>
              
              
              </div>
            </div>
            {this.state.modal}
          </div>
        </div>
      </div>
    );
  }
}
