import React from "react";
import { homeAction } from '../../../../_actions'
import SweetAlert from 'react-bootstrap-sweetalert';
import "../../home.css";
import { SubHeader} from "../../../../_pagebuilder";


let modalAlert;

export class Banner_Main_Home_Add extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      show: false,
      modal: null,
      modalOption: {},
      // messageCreate: '',
      fields: {},
      errors: {},
      isStatus: false,
      status: 0,
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onConfirm = this.onConfirm.bind(this);
    this.onConfirmCreate = this.onConfirmCreate.bind(this);
    this.onCancel = this.onCancel.bind(this);
  }

  componentDidMount() {

  }

  onModalSuccess(head, body) {
    modalAlert = () => (
      <SweetAlert
          style={{display:'block'}}
          success
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="default"
          title={head}
          onConfirm={this.onConfirm}
          >
          {body}
          </SweetAlert>
      );

      this.setState({ show:true, modal: modalAlert() })
  }

  onModalWarning(head, body) {
    modalAlert = () => (
      <SweetAlert
          style={{display:'block'}}
          warning
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="default"
          title={head}
          onConfirm={this.onCancel}
          >
          {body}
          </SweetAlert>
      );

      this.setState({ show:true, modal: modalAlert() })
  }

  onModalError(head, body) {
    modalAlert = () => (
      <SweetAlert
          style={{display:'block'}}
          error
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="default"
          title={head}
          onConfirm={this.onCancel}
          >
          {body}
          </SweetAlert>
      );

      this.setState({ show:true, modal: modalAlert() })
  }

  onModalQuestion(head, body) {
    modalAlert = () => (
    <SweetAlert
        style={{display:'block'}}
        info
        showCancel
        confirmBtnBsStyle="success"
        cancelBtnBsStyle="default"
        title={head}
        onConfirm={() => this.onConfirmCreate()}
        onCancel={() => this.onCancel()}
        >
        {body}
        </SweetAlert>
    );

    this.setState({ show:true, modal: modalAlert() })
  }


  handleSubmit(event) {
    event.preventDefault()
    this.setState({ modalOption: null })

    if (this.validateForm()) {
      this.onModalQuestion('Confirm','คุณต้องการเพิ่มข้อมูล ?')
    }
  }

  onConfirm () {
    this.setState({ modal: null })
    window.location.href="/backoffice/manage-home/banner-main"
  }

  validateForm() {

    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    if (!fields["banner_home_image"]) {
      formIsValid = false;
      errors["banner_home_image"] = "*กรุณาแนบรูปแบนเนอร์";
    }

    if (!fields["banner_home_link"]) {
      formIsValid = false;
      errors["banner_home_link"] = "*กรุณากรอกลิงค์แบนเนอร์";
    }

    this.setState({
      errors: errors
    });
    return formIsValid;
  }

  onConfirmCreate(ev) {
    this.setState({ modal: null })
    
    var formData = new FormData();
    formData.append("banner_home_image", this.state.fields["banner_home_image"]);
    formData.append("banner_home_link", this.state.fields["banner_home_link"]);
    formData.append("is_active", this.state.status);

    homeAction.createBannerMain(formData).then(e => {
      if(e.isSuccess === true) {
        this.onModalSuccess('Success', e.message)
      } else {
        this.onModalError('Error', e.message)
      }
    })
  }

  onCancel(ev) {
    this.setState({ modal: null });
  }

  handleChange = (e) => {
      let { fields, errors } = this.state;
      fields[e.target.name] = e.target.value;
      this.state.errors[e.target.name] = null;
      this.setState({ fields, errors });
  };

  onChangeUploadHandler = e => {
    let { fields, errors } = this.state;
    fields[e.target.name] = e.target.files[0];
    this.state.errors[e.target.name] = null;
    this.setState({
      fields,
      errors
    });
  };

  handleSelectChange = (e, action) => {
    let { fields, errors } = this.state;
    fields[action.name] = e.value;
    this.state.errors[action.name] = null;
    this.setState({ fields, errors });

  };


handleInputChange = e => {

  const target = e.target;
  const name = target.name;

  if(name == "isStatus"){
    this.setState({ isStatus: !this.state.isStatus })
    if(this.state.isStatus == true){
      this.setState({ status: 0 })
    }else{
      this.setState({ status: 1 })
    }
  }

}


  render() {
    return (

      <div>
        <SubHeader 
          main_menu="จัดการหน้า Home" 
          sub_menu="จัดการแบนเนอร์หลัก" 
          sub_menu_link="/backoffice/manage-home/banner-main" 
          sub_menu_function="เพิ่มแบนเนอร์หลัก" />

        <div class="content">
          <div class="card">

            <div class="card-header header-elements-inline">
              <div className="heading-elements">
                <h4 className="panel-title">เพิ่มแบนเนอร์หลัก</h4>

                <div class="card-body">

                  <form className="form-horizontal" onSubmit={(e) => this.handleSubmit(e)}>
                    <fieldset className="content-group">

                      <div className="form-group row">
                        <label className="control-label col-lg-2">รูปภาพแบนเนอร์
                          <br /><span className="text-danger"> (1920*680 px)</span>
                        </label>
                        
                        <div className="col-lg-10">
                          <input 
                            type="file" 
                            className="file-input" 
                            data-show-upload="false" 
                            autoOrientImage="false"
                            name="banner_home_image" 
                            onChange={this.onChangeUploadHandler}
                            />
                          <div className="errorMsg">{this.state.errors.banner_home_image}</div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">ลิงค์แบนเนอร์</label>
                        <div className="col-lg-10">
                          <input
                              name="banner_home_link"
                              type="text"
                              className="form-control"
                              placeholder="ลิงค์แบนเนอร์"
                              onChange={this.handleChange}
                            />
                            <div className="errorMsg">{this.state.errors.banner_home_link}</div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">สถานะแบนเนอร์</label>
                        <div className="col-lg-10">
                          <label className="switch">
                            <input name="isStatus" type="checkbox" onChange={this.handleInputChange} checked={this.state.isStatus}/>
                            <div className="slider"></div>
                          </label>
                        </div>
                      </div>
                    
                      <div className="form-group row">
                        <div className="col-lg-12 text-right">
                          <input
                            type="submit"
                            className="btn btn-success btn-md btn-create"
                            value="บันทึก"
                          />
                        </div>
                      </div>
                    
                    </fieldset>
                  </form>
                </div>
              
              
              </div>
            </div>
            {this.state.modal}
          </div>
        </div>
      </div>
    );
  }
}
