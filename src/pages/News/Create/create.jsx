import React from "react";
import { newsAction } from "../../../_actions";
import SweetAlert from "react-bootstrap-sweetalert";
import Select from "react-select";
import "../news.css";
import { SubHeader } from "../../../_pagebuilder";

let modalAlert;

export class News_Add extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      show: false,
      modal: null,
      selectOption: [],
      fields: {
      },
      errors: {},
      lang: "th",
      active: "ManageNews",
      is_active: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onConfirm = this.onConfirm.bind(this);
    this.onConfirmCreate = this.onConfirmCreate.bind(this);
    this.onCancel = this.onCancel.bind(this);
  }

  componentDidMount() {

  }

  onModalSuccess(head, body) {
    modalAlert = () => (
      <SweetAlert
        style={{ display: "block" }}
        success
        confirmBtnBsStyle="success"
        cancelBtnBsStyle="default"
        title={head}
        onConfirm={this.onConfirm}
      >
        {body}
      </SweetAlert>
    );

    this.setState({ show: true, modal: modalAlert() });
  }

  onModalWarning(head, body) {
    modalAlert = () => (
      <SweetAlert
        style={{ display: "block" }}
        warning
        confirmBtnBsStyle="success"
        cancelBtnBsStyle="default"
        title={head}
        onConfirm={this.onCancel}
      >
        {body}
      </SweetAlert>
    );

    this.setState({ show: true, modal: modalAlert() });
  }

  onModalError(head, body) {
    modalAlert = () => (
      <SweetAlert
        style={{ display: "block" }}
        error
        confirmBtnBsStyle="success"
        cancelBtnBsStyle="default"
        title={head}
        onConfirm={this.onCancel}
      >
        {body}
      </SweetAlert>
    );

    this.setState({ show: true, modal: modalAlert() });
  }

  onModalQuestion(head, body) {
    modalAlert = () => (
      <SweetAlert
        style={{ display: "block" }}
        info
        showCancel
        confirmBtnBsStyle="success"
        cancelBtnBsStyle="default"
        title={head}
        onConfirm={() => this.onConfirmCreate()}
        onCancel={() => this.onCancel()}
      >
        {body}
      </SweetAlert>
    );

    this.setState({ show: true, modal: modalAlert() });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({ modalOption: null });

    if (this.validateForm()) {
      this.onModalQuestion("Confirm", "คุณต้องการเพิ่มข้อมูล ?");
    }
  }

  onConfirm() {
    this.setState({ modal: null });
    window.location.href = "/backoffice/news";
  }

  validateForm() {
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    if (!fields["news_image"]) {
      formIsValid = false;
      errors["news_image"] = "*กรุณาแนบรูปข่าวสาร";
    }

    if (!fields["news_title_th"]) {
      formIsValid = false;
      errors["news_title_th"] = "*กรุณากรอกหัวข่าวสาร (TH)";
    }

    if (!fields["news_title_en"]) {
      formIsValid = false;
      errors["news_title_en"] = "*กรุณากรอกหัวข้อข่าวสาร (EN)";
    }

    if (!fields["news_desc_th"]) {
      formIsValid = false;
      errors["news_desc_th"] = "*กรุณากรอกรายละเอียดข่าวสาร (TH)";
    }

    if (!fields["news_desc_en"]) {
      formIsValid = false;
      errors["news_desc_en"] = "*กรุณากรอกรายละเอียดข่าวสาร (EN)";
    }

    if (!fields["news_date_start"]) {
      formIsValid = false;
      errors["news_date_start"] = "*กรุณากรอกวันที่เริ่มข่าวสาร";
    }

    if (!fields["news_date_end"]) {
      formIsValid = false;
      errors["news_date_end"] = "*กรุณากรอกวันที่สิ้นสุดข่าวสาร";
    }

    this.setState({
      errors: errors,
    });

    return formIsValid;
  }

  onConfirmCreate(ev) {
    this.setState({ modal: null });

    var formData = new FormData();
    formData.append("news_image",this.state.fields["news_image"]);
    formData.append("news_title_th",this.state.fields["news_title_th"]);
    formData.append("news_title_en",this.state.fields["news_title_en"]);
    formData.append("news_desc_th",this.state.fields["news_desc_th"]);
    formData.append("news_desc_en",this.state.fields["news_desc_en"]);
    formData.append("news_date_start",this.state.fields["news_date_start"]);
    formData.append("news_date_end",this.state.fields["news_date_end"]);
    // for (var value of formData.values()) {
    //   console.log(value);
    // }

    newsAction.InsertNews(formData).then((e) => {
      if (e.isSuccess === true) {
        this.onModalSuccess("Success", e.message);
      } else {
        this.onModalError("Error", e.message);
      }
    });
  }

  onCancel(ev) {
    this.setState({ modal: null });
  }


  onHandleCheckChar = (e) =>{
    if(!e.target.value.match(/^[+ฯะัาำิีึืฺุูเแโใไๅๆ็่้๊๋์,ก-ฮa-zA-Z0-9.\s:#/_-]+$/i) && e.target.value.length>0){
       this.onModalError('ไม่สามารถใช้ตัวอักษรพิเศษได้',e.message)
      e.target.value = e.target.value.toString();
    }else{
      return this.handleChange(e)
    }
  }

  handleChange = (e) => {
    if(!e.target.value.match(/^[+ฯะัาำิีึืฺุูเแโใไๅๆ็่้๊๋์,ก-ฮa-zA-Z0-9.\s:#/_-]+$/i) && e.target.value.length>0){
      this.onModalError('ไม่สามารถใช้ตัวอักษรพิเศษได้',e.message)
     e.target.value = e.target.value.toString();
   }else{
    let { fields, errors } = this.state;
    fields[e.target.name] = e.target.value;
    this.state.errors[e.target.name] = null;
    this.setState({ fields, errors });
   }
  };

  getExtension(filename) {
    var parts = filename.split(".");
    return parts[parts.length - 1];
  }

  isImage = (e) => {
    var ext = this.getExtension(e.target.files[0].name);
    switch (ext.toLowerCase()) {
      case "jpg":
      case "jpeg":
      case "png":
        //etc
        // return this.onChangeUploadHandler(e);
        return this.onChangeUploadHandler(e)
    }
    return this.onModalError('.jpg , .jpeg , .png Only',e.message);
    
  };

  onChangeUploadHandler(e) {
    let { fields, errors } = this.state;
    fields[e.target.name] = e.target.files[0];
    this.state.errors[e.target.name] = null;
    this.setState({
      fields,
      errors,
    });
  }

  handleSelectChange = (e, action) => {
    let { fields, errors } = this.state;
    fields[action.name] = e.value;
    this.state.errors[action.name] = null;
    this.setState({ fields, errors });
  };

  handleInputChange = (e) => {
    const target = e.target;
    const name = target.name;
    if (name == "is_active") {
      this.setState({ is_active: !this.state.is_active });
    }
  };

  render() {

    return (
      <div>
        <SubHeader
          main_menu_name="จัดการข่าวสาร"
          main_menu_link=""
          sub_menu_name="รายการข่าวสาร"
          sub_menu_link="/backoffice/promotion"
          sub_menu_function="เพิ่มข่าวสาร"
        />

        <div className="content">
          <div className="card">
            <div className="card-header header-elements-inline">
              <div className="heading-elements">
                <h4 className="panel-title">เพิ่มข่าวสาร</h4>

                <div className="card-body">
                  <form
                    className="form-horizontal"
                    name="FormCreate"
                    onSubmit={(e) => this.handleSubmit(e)}
                  >
                    <fieldset className="content-group">
                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                          รูปภาพข่าวสาร{" "}
                          <div className="text-danger"></div>
                        </label>

                        <div className="col-lg-10">
                          <input
                            type="file"
                            className="file-input"
                            data-show-upload="false"
                            autoOrientImage="false"
                            name="news_image"
                            onChange={this.isImage}
                            accept=".jpg , .png ,.jpeg"
                          />
                          <div className="errorMsg">
                            {this.state.errors.news_image}
                          </div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                          หัวข้อข่าวสาร (TH)
                        </label>
                        <div className="col-lg-10">
                          <input
                            name="news_title_th"
                            type="text"
                            className="form-control"
                            placeholder="หัวข้อข่าวสาร (TH)"
                            value = {this.state.fields["news_title_th"] || ""}
                            onChange={this.onHandleCheckChar}
                          />
                          <div className="errorMsg">
                            {this.state.errors.news_title_th}
                          </div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                          หัวข้อข่าวสาร (EN)
                        </label>
                        <div className="col-lg-10">
                          <input
                            name="news_title_en"
                            type="text"
                            className="form-control"
                            placeholder="หัวข้อข่าวสาร (EN)"
                            value = {this.state.fields["news_title_en"] || ""}
                            onChange={this.onHandleCheckChar}
                          />
                          <div className="errorMsg">
                            {this.state.errors.news_title_en}
                          </div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                           รายละเอียดข่าวสาร (TH)
                        </label>
                        <div className="col-lg-10">
                          <textarea 
                            name="news_desc_th" 
                            className="form-control"
                            value = {this.state.fields["news_desc_th"] || ""}
                            rows="5"
                            onChange={this.onHandleCheckChar} 
                            />
                          <div className="errorMsg">
                            {this.state.errors.news_desc_th}
                          </div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                           รายละเอียดข่าวสาร (EN)
                        </label>
                        <div className="col-lg-10">
                          <textarea 
                            name="news_desc_en" 
                            className="form-control"
                            value = {this.state.fields["news_desc_en"] || ""}
                            rows="5"
                            onChange={this.onHandleCheckChar} 
                            />
                          <div className="errorMsg">
                            {this.state.errors.news_desc_en}
                          </div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">
                          วันที่เริ่มข่าวสาร
                        </label>
                        <div className="col-lg-4">
                          <input
                            name="news_date_start"
                            type="date"
                            className="form-control"
                            placeholder="วันที่เริ่มข่าวสาร"
                            value = {this.state.fields["news_date_start"] || ""}
                            onChange={this.onHandleCheckChar}
                          />
                          <div className="errorMsg">
                            {this.state.errors.news_date_start}
                          </div>
                        </div>

                        <label className="control-label col-lg-2">
                          วันที่สิ้นสุดข่าวสาร
                        </label>
                        <div className="col-lg-4">
                          <input
                            name="news_date_end"
                            type="date"
                            className="form-control"
                            placeholder="วันที่สิ้นสุดข่าวสาร"
                            value = {this.state.fields["news_date_end"] || ""}
                            onChange={this.onHandleCheckChar}
                          />
                          <div className="errorMsg">
                            {this.state.errors.news_date_end}
                          </div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <div className="col-lg-12 text-right">
                          <input
                            type="submit"
                            className="btn btn-success btn-md btn-create"
                            value="บันทึก"
                          />
                        </div>
                      </div>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
            {this.state.modal}
          </div>
        </div>
      </div>
    );
  }
}
