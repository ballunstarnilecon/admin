import React from "react";
import { adminAction } from '../../../_actions'
import SweetAlert from 'react-bootstrap-sweetalert';
import "../admin.css";
import { SubHeader} from "../../../_pagebuilder";
var md5 = require('md5');

let modalAlert;

export class Admin_Add extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      show: false,
      modal: null,
      modalOption: {},
      fields: {},
      errors: {},
      isStatus: false,
      status: 0,
      lang: 'th',
      active: 'ManageAdmin',

    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onConfirm = this.onConfirm.bind(this);
    this.onConfirmCreate = this.onConfirmCreate.bind(this);
    this.onCancel = this.onCancel.bind(this);
  }

  componentDidMount() {

  }

  onModalSuccess(head, body) {
    modalAlert = () => (
      <SweetAlert
          style={{display:'block'}}
          success
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="default"
          title={head}
          onConfirm={this.onConfirm}
          >
          {body}
          </SweetAlert>
      );

      this.setState({ show:true, modal: modalAlert() })
  }

  onModalWarning(head, body) {
    modalAlert = () => (
      <SweetAlert
          style={{display:'block'}}
          warning
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="default"
          title={head}
          onConfirm={this.onCancel}
          >
          {body}
          </SweetAlert>
      );

      this.setState({ show:true, modal: modalAlert() })
  }

  onModalError(head, body) {
    modalAlert = () => (
      <SweetAlert
          style={{display:'block'}}
          error
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="default"
          title={head}
          onConfirm={this.onCancel}
          >
          {body}
          </SweetAlert>
      );

      this.setState({ show:true, modal: modalAlert() })
  }

  onModalQuestion(head, body) {
    modalAlert = () => (
    <SweetAlert
        style={{display:'block'}}
        info
        showCancel
        confirmBtnBsStyle="success"
        cancelBtnBsStyle="default"
        title={head}
        onConfirm={() => this.onConfirmCreate()}
        onCancel={() => this.onCancel()}
        >
        {body}
        </SweetAlert>
    );

    this.setState({ show:true, modal: modalAlert() })
  }


  handleSubmit(event) {
    event.preventDefault()
    this.setState({ modalOption: null })

    if (this.validateForm()) {
      this.onModalQuestion('Confirm','คุณต้องการเพิ่มข้อมูล ?')
    }
  }

  onConfirm () {
    this.setState({ modal: null })
    window.location.href="/backoffice/admin"
  }

  validateForm() {
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    if (!fields["manager_dept"]) {
      formIsValid = false;
      errors["manager_dept"] = "*กรุณากรอกชื่อฝ่าย";
    }

    if (!fields["manager_role"]) {
      formIsValid = false;
      errors["manager_role"] = "*กรุณาเลือกสิทธิ์ผู้ใช้ระบบ";
    }

    if (!fields["manager_username"]) {
      formIsValid = false;
      errors["manager_username"] = "*กรุณากรอกชื่อผู้ใช้ระบบ";
    }

    if (!fields["manager_password"]) {
      formIsValid = false;
      errors["manager_password"] = "*กรุณากรอกรหัสผ่าน";
    }

    if (!fields["admin_confirm_password"]) {
      formIsValid = false;
      errors["admin_confirm_password"] = "*กรุณากรอกยืนยันรหัสผ่าน";
    }

    if (fields["manager_password"] != fields["admin_confirm_password"]) {
      formIsValid = false;
      errors["admin_confirm_password"] = "*รหัสผ่านไม่ตรงกัน";
    }

    if (fields["manager_password"] && fields["manager_password"].length < 8) {
      formIsValid = false;
      errors["manager_password"] = "*ตัวอักษรต้องมากกว่า 8 ตัว";
    }

    if (fields["admin_confirm_password"] && fields["admin_confirm_password"].length < 8) {
      formIsValid = false;
      errors["admin_confirm_password"] = "*ตัวอักษรต้องมากกว่า 8 ตัว";
    }

    this.setState({
      errors: errors
    });
    return formIsValid;
  }

  onConfirmCreate(ev) {
    this.setState({ modal: null })

    let data = {
      "manager_dept": this.state.fields["manager_dept"],
      "manager_role": this.state.fields["manager_role"],
      "manager_username": this.state.fields["manager_username"],
      "manager_password": this.state.fields["manager_password"],
      "is_active": this.state.status
    }
    console.log(data);
    adminAction.InsertAdmin(data, this.state.lang).then(e => {
      console.log(e.data);
      if(e.data.isSuccess === true) {
        this.onModalSuccess('Success', e.message)
      } else {
        this.onModalError('Error', e.message)
      }
    })
  }

  onCancel(ev) {
    this.setState({ modal: null });
  }

  handleChange = (e) => {
      let { fields, errors } = this.state;
      fields[e.target.name] = e.target.value;
      this.state.errors[e.target.name] = null;
      this.setState({ fields, errors });
  };

  onChangeUploadHandler = e => {
    let { fields, errors } = this.state;
    fields[e.target.name] = e.target.files[0];
    this.state.errors[e.target.name] = null;
    this.setState({
      fields,
      errors
    });
  };

  handleSelectChange = (e) => {
    let { fields, errors } = this.state;
    fields[e.target.name] = e.target.value;
    this.state.errors[e.target.name] = null;
    this.setState({ fields, errors });
  };


handleInputChange = e => {

  const target = e.target;
  const name = target.name;

  if(name == "isStatus"){
    this.setState({ isStatus: !this.state.isStatus })
    if(this.state.isStatus == true){
      this.setState({ status: 0 })
    }else{
      this.setState({ status: 1 })
    }
  }

}


  render() {
    var manager_role = [
      { value: "Admin", label: "Admin" },
      { value: "Editor", label: "Editor" }
    ]
    var option_manager_role = []
    for(var i in manager_role){
      option_manager_role.push(
        <option value={manager_role[i].value}>{manager_role[i].label}</option>
      )

    }
    return (

      <div>
        <SubHeader 
          main_menu_name="จัดการผู้ใช้ระบบ" 
          main_menu_link="/backoffice/admin" 
          sub_menu_name="" 
          sub_menu_link="" 
          sub_menu_function="เพิ่มผู้ใช้ระบบ" />

        <div className="content">
          <div className="card">

            <div className="card-header header-elements-inline">
              <div className="heading-elements">
                <h4 className="panel-title">เพิ่มผู้ใช้ระบบ</h4>

                <div className="card-body">

                  <form className="form-horizontal" onSubmit={(e) => this.handleSubmit(e)}>
                    <fieldset className="content-group">
                      <div className="form-group row">
                        <label className="control-label col-lg-2">ชื่อฝ่าย</label>
                        <div className="col-lg-4">
                          <input
                              name="manager_dept"
                              type="text"
                              className="form-control"
                              placeholder="ชื่อฝ่าย"
                              onChange={this.handleChange}
                            />
                            <div className="errorMsg">{this.state.errors.manager_dept}</div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">สิทธิ์ผู้ใช้ระบบ</label>
                        <div className="col-lg-4">
                          <select className="form-control" name="manager_role" onChange={(e) => this.handleSelectChange(e)}>
                            <option value="">กรุณาเลือก</option>
                            {option_manager_role}
                          </select>
                          <div className="errorMsg">{this.state.errors.manager_role}</div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">ชื่อผู้ใช้ระบบ (Username)</label>
                        <div className="col-lg-4">
                          <input
                              name="manager_username"
                              type="text"
                              className="form-control"
                              placeholder="ชื่อผู้ใช้ระบบ"
                              onChange={this.handleChange}
                            />
                            <div className="errorMsg">{this.state.errors.manager_username}</div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">รหัสผ่าน</label>
                        <div className="col-lg-4">
                          <input
                              name="manager_password"
                              type="password"
                              className="form-control"
                              placeholder="รหัสผ่าน"
                              onChange={this.handleChange}
                            />
                            <div className="errorMsg">{this.state.errors.manager_password}</div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">ยืนยันรหัสผ่าน</label>
                        <div className="col-lg-4">
                          <input
                              name="admin_confirm_password"
                              type="password"
                              className="form-control"
                              placeholder="ยืนยันรหัสผ่าน"
                              onChange={this.handleChange}
                            />
                            <div className="errorMsg">{this.state.errors.admin_confirm_password}</div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">สถานะผู้ใช้ระบบ</label>
                        <div className="col-lg-10">
                          <label className="switch">
                            <input name="isStatus" type="checkbox" onChange={this.handleInputChange} checked={this.state.isStatus}/>
                            <div className="slider"></div>
                          </label>
                        </div>
                      </div>
                    
                      <div className="form-group row">
                        <div className="col-lg-12 text-right">
                          <input
                            type="submit"
                            className="btn btn-success btn-md btn-create"
                            value="บันทึก"
                          />
                        </div>
                      </div>
                    
                    </fieldset>
                  </form>
                </div>
              
              
              </div>
            </div>
            {this.state.modal}
          </div>
        </div>
      </div>
    );
  }
}
