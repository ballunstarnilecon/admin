import React from "react";
import { adminAction } from '../../../_actions'
import SweetAlert from 'react-bootstrap-sweetalert';
import "../admin.css";
import { SubHeader} from "../../../_pagebuilder";
import $ from "jquery";
// var moment = require('moment'); // require
import moment from "moment"
import 'moment/locale/th';
var md5 = require('md5');

let modalAlert;

export class Admin_Edit extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      show: false,
      modal: null,
      modalOption: {},
      fields: {},
      errors: {},
      isStatus: 0,
      status: 0,
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onConfirm = this.onConfirm.bind(this);
    this.onConfirmUpdate = this.onConfirmUpdate.bind(this);
    this.onCancel = this.onCancel.bind(this);
  }

  componentDidMount() {
    const {
      match: { params }
    } = this.props;
    this.getAdminByID(params.manager_id);
  }

  getAdminByID(manager_id) {
    adminAction.getAdmin(manager_id).then(e => {
      console.log(e.data.data[0]);
      if (e.data.isSuccess === true) {
        let data = e.data.data[0]
        this.setState({
          fields: {
            ...this.state.fields,
            "manager_id": data.manager_id,
            "manager_dept": data.manager_dept,
            "manager_role": data.manager_role,
            "manager_username": data.manager_username,
            "manager_password": data.manager_password,
            "updated_at": moment(data.updated_at,'DD-MM-YYYY , HH:mm:ss').locale('th').format('DD/MM/YYYY HH:mm:ss'),
            "is_active": data.is_active

          }
        });

      } else {
        this.onModalError("Error", "ไม่พบข้อมูลดังกล่าว");
      }
    });
  }

  onModalSuccess(head, body) {
    modalAlert = () => (
      <SweetAlert
          style={{display:'block'}}
          success
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="default"
          title={head}
          onConfirm={this.onConfirm}
          >
          {body}
          </SweetAlert>
      );

      this.setState({ show:true, modal: modalAlert() })
  }

  onModalWarning(head, body) {
    modalAlert = () => (
      <SweetAlert
          style={{display:'block'}}
          warning
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="default"
          title={head}
          onConfirm={this.onCancel}
          >
          {body}
          </SweetAlert>
      );

      this.setState({ show:true, modal: modalAlert() })
  }

  onModalError(head, body) {
    modalAlert = () => (
      <SweetAlert
          style={{display:'block'}}
          error
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="default"
          title={head}
          onConfirm={this.onCancel}
          >
          {body}
          </SweetAlert>
      );

      this.setState({ show:true, modal: modalAlert() })
  }

  onModalQuestion(head, body) {
    modalAlert = () => (
    <SweetAlert
        style={{display:'block'}}
        info
        showCancel
        confirmBtnBsStyle="success"
        cancelBtnBsStyle="default"
        title={head}
        onConfirm={() => this.onConfirmUpdate()}
        onCancel={() => this.onCancel()}
        >
        {body}
        </SweetAlert>
    );

    this.setState({ show:true, modal: modalAlert() })
  }


  handleSubmit(event) {
    event.preventDefault()
    this.setState({ modalOption: null })

    if (this.validateForm()) {
      this.onModalQuestion('Confirm','คุณต้องการแก้ไขข้อมูล ?')
    }
  }

  onConfirm () {
    this.setState({ modal: null })
    window.location.href="/backoffice/admin"
  }

  validateForm() {

    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    if (!fields["manager_dept"]) {
      formIsValid = false;
      errors["manager_dept"] = "*กรุณากรอกชื่อฝ่าย";
    }

    if (!fields["manager_role"]) {
      formIsValid = false;
      errors["manager_role"] = "*กรุณาเลือกสิทธิ์ผู้ใช้ระบบ";
    }

    if (!fields["manager_username"]) {
      formIsValid = false;
      errors["manager_username"] = "*กรุณากรอกชื่อผู้ใช้ระบบ";
    }

    if (!fields["manager_password"]) {
      formIsValid = false;
      errors["manager_password"] = "*กรุณากรอกรหัสผ่าน";
    }

    if (!fields["admin_confirm_password"]) {
      formIsValid = false;
      errors["admin_confirm_password"] = "*กรุณากรอกยืนยันรหัสผ่าน";
    }

    if (fields["manager_password"] != fields["admin_confirm_password"]) {
      formIsValid = false;
      errors["admin_confirm_password"] = "*รหัสผ่านไม่ตรงกัน";
    }

    if (fields["manager_password"] && fields["manager_password"].length < 8) {
      formIsValid = false;
      errors["manager_password"] = "*ตัวอักษรต้องมากกว่า 8 ตัว";
    }

    if (fields["admin_confirm_password"] && fields["admin_confirm_password"].length < 8) {
      formIsValid = false;
      errors["admin_confirm_password"] = "*ตัวอักษรต้องมากกว่า 8 ตัว";
    }

    this.setState({
      errors: errors
    });
    return formIsValid;
  }

  onConfirmUpdate(ev) {
    this.setState({ modal: null })
    let id = this.state.fields['manager_id'];
    console.log(this.state.fields["is_active"]);
    let data = {
      "manager_dept": this.state.fields["manager_dept"],
      "manager_role": this.state.fields["manager_role"],
      "manager_username": this.state.fields["manager_username"],
      "manager_password": md5(this.state.fields["manager_password"]),
      "is_active": this.state.fields["is_active"]
    }
    console.log(data);
    adminAction.updateAdmin(data, id, 'th').then(e => {
      if(e.isSuccess === true) {
        this.onModalSuccess('Success', e.message)
      } else {
        this.onModalError('Error', e.message)
      }
    })
  }

  onCancel(ev) {
    this.setState({ modal: null });
  }

  handleChange = (e) => {
      let { fields, errors } = this.state;
      fields[e.target.name] = e.target.value;
      this.state.errors[e.target.name] = null;
      this.setState({ fields, errors });
  };

  onChangeUploadHandler = e => {
    let { fields, errors } = this.state;
    fields[e.target.name] = e.target.files[0];
    this.state.errors[e.target.name] = null;
    this.setState({
      fields,
      errors
    });
  };

  handleSelectChange = (e) => {
    let { fields, errors } = this.state;
    fields[e.target.name] = e.target.value;
    this.state.errors[e.target.name] = null;
    this.setState({ fields, errors });
  };


handleInputChange = e => {

  const target = e.target;
  const name = target.name;
  let { fields, errors } = this.state;

  if(name == "is_active"){
    fields[e.target.name] = e.target.checked === true ? 1 : 0;
  }
  this.setState({ fields });
}


  render() {
    var manager_role = [
      { value: "Admin", label: "Admin" },
      { value: "Editor", label: "Editor" }
    ]

    var option_manager_role = []
    for(var i in manager_role){
      option_manager_role.push(
        <option value={manager_role[i].value}>{manager_role[i].label}</option>
      )
    }
    return (

      <div>
        <SubHeader 
          main_menu_name="จัดการผู้ใช้ระบบ" 
          main_menu_link="/backoffice/admin" 
          sub_menu_name="" 
          sub_menu_link="" 
          sub_menu_function="แก้ไขผู้ใช้ระบบ" />

        <div class="content">
          <div class="card">

            <div class="card-header header-elements-inline">
              <div className="heading-elements">
                <h4 className="panel-title">แก้ไขผู้ใช้ระบบ</h4>

                <div class="card-body">

                  <form className="form-horizontal" onSubmit={(e) => this.handleSubmit(e)}>
                    <fieldset className="content-group">
                    <div className="form-group row">
                        <label className="control-label col-lg-2">ชื่อฝ่าย</label>
                        <div className="col-lg-4">
                          <input
                              name="manager_dept"
                              type="text"
                              className="form-control"
                              placeholder="ชื่อฝ่าย"
                              value={this.state.fields["manager_dept"] || ""}
                              onChange={this.handleChange}
                            />
                            <div className="errorMsg">{this.state.errors.manager_dept}</div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">สิทธิ์ผู้ใช้ระบบ</label>
                        <div className="col-lg-4">
                          <select className="form-control" name="manager_role" value={this.state.fields['manager_role']} onChange={(e) => this.handleSelectChange(e)}>
                            <option value="">กรุณาเลือก</option>
                            {option_manager_role}
                          </select>
                          <div className="errorMsg">{this.state.errors.manager_role}</div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">ชื่อผู้ใช้ระบบ (Username)</label>
                        <div className="col-lg-4">
                          <input
                              name="manager_username"
                              type="text"
                              className="form-control"
                              placeholder="ชื่อผู้ใช้ระบบ"
                              value={this.state.fields["manager_username"] || ""}
                              onChange={this.handleChange}
                            />
                            <div className="errorMsg">{this.state.errors.manager_username}</div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">รหัสผ่าน</label>
                        <div className="col-lg-4">
                          <input
                              name="manager_password"
                              type="password"
                              className="form-control"
                              placeholder="รหัสผ่าน"
                              value={this.state.fields["manager_password"] || ""}
                              onChange={this.handleChange}
                            />
                            <div className="errorMsg">{this.state.errors.manager_password}</div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">ยืนยันรหัสผ่าน</label>
                        <div className="col-lg-4">
                          <input
                              name="admin_confirm_password"
                              type="password"
                              className="form-control"
                              placeholder="ยืนยันรหัสผ่าน"
                              onChange={this.handleChange}
                            />
                            <div className="errorMsg">{this.state.errors.admin_confirm_password}</div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">สถานะผู้ใช้ระบบ</label>
                        <div className="col-lg-10">
                          <label className="switch">
                            <input 
                              name="is_active"
                              type="checkbox"
                              onChange={this.handleInputChange}
                              checked={this.state.fields['is_active']}/>
                            <div className="slider"></div>
                          </label>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">วันที่อัพเดต</label>
                        <div className="col-lg-10">
                          <span>{this.state.fields.updated_at}</span>
                        </div>
                      </div>

                      <div className="form-group row">
                        <div className="col-lg-12 text-right">
                          <input
                            type="submit"
                            className="btn btn-success btn-md btn-create"
                            value="บันทึก"
                          />
                        </div>
                      </div>

                      </fieldset>
                    
                    
                    {/* <fieldset className="content-group">
                      <div className="text-center text-center">
                          <img
                            className="preview-img"
                            src={this.state.fields["banner_home_image_show"] || ""}
                          />
                        </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">รูปภาพแบนเนอร์
                          <br /><span className="text-danger"> (205*40 px)</span>
                        </label>
                        
                        <div className="col-lg-10">
                          <input 
                            type="file" 
                            className="file-input" 
                            data-show-upload="false" 
                            autoOrientImage="false"
                            name="banner_home_image" 
                            
                            onChange={this.onChangeUploadHandler}
                            />
                          <div className="errorMsg">{this.state.errors.banner_home_image}</div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">ลิงค์แบนเนอร์</label>
                        <div className="col-lg-10">
                          <input
                              name="banner_home_link"
                              type="text"
                              className="form-control"
                              placeholder="ลิงค์แบนเนอร์"
                              value={this.state.fields["banner_home_link"] || ""}
                              onChange={this.handleChange}
                            />
                            <div className="errorMsg">{this.state.errors.banner_home_link}</div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="control-label col-lg-2">สถานะแบนเนอร์</label>
                        <div className="col-lg-10">
                          <label className="switch">
                            <input 
                              name="isActive" 
                              type="checkbox" 
                              onChange={this.handleInputChange} 
                              checked={this.state.fields.isActive}/>
                            <div className="slider"></div>
                          </label>
                        </div>
                      </div>
                    
                      <div className="form-group row">
                        <label className="control-label col-lg-2">วันที่อัพเดต</label>
                        <div className="col-lg-10">
                          <span>{this.state.fields.updated_at}</span>
                        </div>
                      </div>
                    

                      <div className="form-group row">
                        <div className="col-lg-12 text-right">
                          <input
                            type="submit"
                            className="btn btn-success btn-md btn-update"
                            value="บันทึก"
                          />
                        </div>
                      </div>
                    
                    </fieldset> */}
                  </form>
                </div>
              
              
              </div>
            </div>
            {this.state.modal}
          </div>
        </div>
      </div>
    );
  }
}
