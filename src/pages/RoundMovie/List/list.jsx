import React from "react";
import $ from "jquery";
import { roundMovieAction } from "../../../_actions";
import { SubHeader} from "../../../_pagebuilder";
import { Base_API } from "../../../_constants/matcher";
import moment from "moment"
import 'moment/locale/th';

import SweetAlert from "react-bootstrap-sweetalert";
import "../movie.css";
import { movieAction } from "../../../_actions/movie.action";

$.DataTable = require("datatables.net");
let modalAlert;

export class RoundMovie_List extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.openModalHandler = this.openModalHandler.bind(this);
    this.closeModalHandler = this.closeModalHandler.bind(this);
    this.onConfirm = this.onConfirm.bind(this);
    this.onCancel = this.onCancel.bind(this);

    this.state = {
      show: false,
      modal: null,
      isModal: false,
      lang: 'th',
      active: 'ManageRoundMovie',
    };
  }

  openModalHandler() {
    this.setState({
      isModal: true
    });
  }

  closeModalHandler = () => {
    this.setState({
      isModal: false
    });
  };

  componentDidMount() {
    sessionStorage.setItem("active", "ManageRoundMovie");

    const columns = [
      {
        title: "Session_strID",
        class: "text-center",
        data: "Session_strID"
      },
      {
        title: "Session_dtmDate_Time",
        class: "text-center",
        data: "Session_dtmDate_Time"
      },
      {
        title: "Screen_strName",
        class: "text-center",
        data: "Screen_strName"
      },
      {
        title: "Session_intID",
        class: "text-center",
        data: "Session_intID"
      },
      {
        title: "Movie_strID",
        class: "text-center",
        data: "Movie_strID"
      },
      {
        title: "Movie_strName",
        class: "text-left",
        data: "Movie_strName"
      }
      // {
      //   title: "จัดการ",
      //   class: "text-center",
      //   data: "Session_strID"
      // }
    ];
    let table = $(this.refs.main).DataTable({
      columnDefs: [
        // {
        //   targets: 6,
        //   createdCell: function(td, cellData, rowData, row, col) {
        //     let button = `<div class="list-icons">
        //     <div class="dropdown">
        //       <a href="#" class="list-icons-item" data-toggle="dropdown">
        //         <i class="icon-menu9"></i>
        //       </a>

        //       <div class="dropdown-menu dropdown-menu-right">
        //         <a href="/backoffice/movie/edit/${cellData}" class="dropdown-item">
        //           <i class="icon-pencil3 mr-3 mb-1"></i> แก้ไข
        //         </a>
        //         <button type='button' class="dropdown-item" name='delete_btn' data-content='${cellData}'>
        //           <i class="icon-trash mr-3 mb-1"></i>ลบ
        //         </button>
        //       </div>
        //     </div>
        //   </div>
        //   `
        //     $(td).html(button);
        //   },
        //   orderable: false
        // }
      ],
      ajax: {
        url: `${Base_API.url_api}/ListMovieShowtimes`,
        type: "GET",
        dataType: "JSON"
      },
      order: [[1, "DESC"]],
      columns,
      serverSide: true,
      ordering: true,
      searching: true,
      processing: true,
      bLengthChange: false,
      "language": {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span> '
      },
    });

    var state = this;
    

    $(this.refs.main).on("click", ".activeBtn", function () {
      var id = $(this).data("id");
      var fval = $(this).is(":checked") === true ? 1 : 0;

      var formData = new FormData();
      formData.append("is_active", fval);

      // packageAction.updatePackage(formData, id).then(e => {
      //   if (e.isSuccess === false) {
      //     onModalError("Error", e.message);
      //   } else {
      //     onModalSuccess("Success", e.message);
      //   }
      // });
    });

    $(this.refs.main).on("click", 'button[name="delete_btn"]', function() {
      var id = $(this).data("content");
      modalAlert = () => (
        <SweetAlert
          style={{ display: "block" }}
          info
          showCancel
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="danger"
          title="Delete"
          onConfirm={() => onConfirmDelete(id)}
          onCancel={() => onConfirm()}
        >
          คุณต้องการลบข้อมูลนี้หรือไม่ ?
        </SweetAlert>
      );

      state.setState({ show: true, modal: modalAlert() });
    });

    function onConfirmDelete(id) {   
    //   movieAction.DeleteMovie(id).then(e => {
    //     if (e.isSuccess === false) {
    //       onModalError("Error", e.message);
    //     } else {
    //       onModalSuccess("Success", e.message);
    //       table.ajax.reload();
    //     }
    //   });
    }

    function onModalSuccess(head, body) {
      modalAlert = () => (
        <SweetAlert
          style={{ display: "block" }}
          success
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="default"
          title={head}
          onConfirm={onConfirm}
        >
          {body}
        </SweetAlert>
      );

      state.setState({ show: true, modal: modalAlert() });
    }

    function onModalError(head, body) {
      modalAlert = () => (
        <SweetAlert
          style={{ display: "block" }}
          error
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="default"
          title={head}
          onConfirm={onConfirm}
        >
          {body}
        </SweetAlert>
      );

      state.setState({ show: true, modal: modalAlert() });
    }

    function onConfirm(ev) {
      state.setState({ modal: null });
    }
  }

  insertMovieShowTime() {
    roundMovieAction.InsertMovieShowtimes().then(e => {
      if(e.isSuccess === true) {
        this.onModalSuccess('Success', e.message)
      } else {
        this.onModalError('Error', e.message)
      }
    })
  }

  onModalSuccess(head, body) {
    modalAlert = () => (
      <SweetAlert
          style={{display:'block'}}
          success
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="default"
          title={head}
          onConfirm={this.onConfirm}
          >
          {body}
          </SweetAlert>
      );

      this.setState({ show:true, modal: modalAlert() })
  }

  onModalError(head, body) {
    modalAlert = () => (
      <SweetAlert
          style={{display:'block'}}
          error
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="default"
          title={head}
          onConfirm={this.onCancel}
          >
          {body}
          </SweetAlert>
      );

      this.setState({ show:true, modal: modalAlert() })
  }

  onConfirm () {
    this.setState({ modal: null })
    window.location.reload()
  }

  onCancel(ev) {
    this.setState({ modal: null });
  }

  render() {
    return (
      <div>
        <SubHeader 
          main_menu_name="จัดการรอบภาพยนต์" 
          main_menu_link="" 
          sub_menu_name="รายการรอบภาพยนต์" 
          sub_menu_link="" 
          sub_menu_function="" />

        <div class="content">
          <div class="card">
            <div class="card-header header-elements-inline text-right">
              <div className="heading-elements">
                <button
                  type="button"
                  name="btn_add"
                  data-content=""
                  className="btn btn-black-embassy"
                  onClick={()=>this.insertMovieShowTime()}
                >
                  ดึงข้อมูลรอบหนัง
                </button>

              </div>
            </div>
            <div class="card-body">
              <div className="table-responsive">
                <table id="dataTable" className="table table-hover" ref="main" />
              </div>
              {this.state.modal}
            </div>
          </div>
        </div>
        
      </div>
    );
  }
}
