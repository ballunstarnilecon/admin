import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { PrivateRoute } from '../_components'

import LoginPage from '../pages/login';
import Dashboard from '../pages/Dashboard';
import { Movie_List, Movie_Add, Movie_Edit } from '../pages/Movie';
import { RoundMovie_List } from '../pages/RoundMovie';
import { Banner_List, Banner_Add, Banner_Edit } from '../pages/Banner';
import { Promotion_List, Promotion_Add, Promotion_Edit } from '../pages/Promotion';
import { News_List, News_Add, News_Edit } from '../pages/News';
import { Admin_List, Admin_Add, Admin_Edit } from '../pages/Admin';
import NotFoundPages from '../pages/404'
const chLogin = (localStorage.getItem('login-backoffice-embassy') === null ? LoginPage : Dashboard);

export default () => (
    <Switch>
        <Route exact path="/" component={chLogin} />
        <Route exact path="/backoffice" component={chLogin} />
        <Route exact path="/backoffice/login" component={chLogin} />
        <PrivateRoute exact path="/backoffice/homepage" component={Dashboard} />
        
        {/* <PrivateRoute exact path="/backoffice/manage-home/banner-main" component={Banner_Main_Home_List} />
        <PrivateRoute exact path="/backoffice/manage-home/banner-main/add" component={Banner_Main_Home_Add} />
        <PrivateRoute exact path="/backoffice/manage-home/banner-main/edit/:banner_home_id" component={Banner_Main_Home_Edit} />

        <PrivateRoute exact path="/backoffice/manage-home/banner-sub/edit/:banner_sub_home_id" component={Banner_Sub_Home_Edit} /> */}

        <PrivateRoute exact path="/backoffice/movie" component={Movie_List} />
        <PrivateRoute exact path="/backoffice/movie/add" component={Movie_Add} />
        <PrivateRoute exact path="/backoffice/movie/edit/:movie_id" component={Movie_Edit} />

        <PrivateRoute exact path="/backoffice/roundmovie" component={RoundMovie_List} />

        <PrivateRoute exact path="/backoffice/banner" component={Banner_List} />
        <PrivateRoute exact path="/backoffice/banner/add" component={Banner_Add} />
        <PrivateRoute exact path="/backoffice/banner/edit/:banner_id" component={Banner_Edit} />

        <PrivateRoute exact path="/backoffice/promotion" component={Promotion_List} />
        <PrivateRoute exact path="/backoffice/promotion/add" component={Promotion_Add} />
        <PrivateRoute exact path="/backoffice/promotion/edit/:promotion_id" component={Promotion_Edit} />

        <PrivateRoute exact path="/backoffice/news" component={News_List} />
        <PrivateRoute exact path="/backoffice/news/add" component={News_Add} />
        <PrivateRoute exact path="/backoffice/news/edit/:news_id" component={News_Edit} />

        <PrivateRoute exact path="/backoffice/admin" component={Admin_List} />
        <PrivateRoute exact path="/backoffice/admin/add" component={Admin_Add} />
        <PrivateRoute exact path="/backoffice/admin/edit/:manager_id" component={Admin_Edit} />

        <PrivateRoute component={NotFoundPages} />

    </Switch>
)