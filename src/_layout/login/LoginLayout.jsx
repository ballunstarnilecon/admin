import React, { Component } from "react";
import Routing from "../../routes";
import "./login.css"

export class LoginLayout extends Component {
  render() {
    return (
      <div className="login-container">
        <div className="page-container">
          <div className="page-content">
            <div className="content-wrapper">
              <Routing></Routing>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
