import React from "react";
import { Redirect } from "react-router";
import { NavLink } from "react-router-dom";
import "./header.css";

export class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      logout: false
    };
  }

  render() {
    const { logout } = this.state;
    if (logout) {
      localStorage.clear();
      window.location.href='/backoffice'
    }

    var dept_3bb = localStorage.getItem('dept-3bb')

    return (
      <div>
        
        <div className="navbar navbar-expand-md navbar-dark bg-dark-embassy">
          <div className="navbar-brand text-center">
            <a href="../full/index.html" className="d-inline-block">
              <img src={process.env.PUBLIC_URL+'/images/3BB_logo@3x.png'} alt="" className="logo-login"></img>
            </a>
          </div>

          <div className="d-md-none">
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
              <i className="icon-tree5"></i>
            </button>
            <button className="navbar-toggler sidebar-mobile-main-toggle" type="button">
              <i className="icon-paragraph-justify3"></i>
            </button>
          </div>

          <div className="collapse navbar-collapse" id="navbar-mobile">
            <ul className="navbar-nav">
              {/* <li className="nav-item">
                <a href="#" className="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                  <i className="icon-paragraph-justify3"></i>
                </a>
              </li> */}
            </ul>

            <ul className="navbar-nav ml-auto">
              <li className="nav-item dropdown dropdown-user">
                <a href="#" className="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                  <img src="../../../../global_assets/images/image.png" className="rounded-circle mr-2" height="34" alt="" />
                  <span>{dept_3bb}</span>
                </a>

                <div className="dropdown-menu dropdown-menu-right">
                  <a href="#" className="dropdown-item" onClick={() => this.setState({ logout: true })}><i className="icon-switch2"></i> Logout</a>
                </div>
              </li>
            </ul>
          </div>
        </div>

      </div>
    );
  }
}
