import React, { Component } from "react";

export class SubHeader extends Component {
    render() {
        var main_menu_name = this.props.main_menu_name
        var main_menu_link = this.props.main_menu_link
        var sub_menu_name = this.props.sub_menu_name
        var sub_menu_link = this.props.sub_menu_link
        var sub_menu_function = this.props.sub_menu_function

        var breadcrumb,breadcrumb_function,main_menu
        if(sub_menu_function && sub_menu_name){
            breadcrumb = <a href={sub_menu_link} class="breadcrumb-item">{sub_menu_name}</a>
            breadcrumb_function = <span class="breadcrumb-item active">{sub_menu_function}</span>
        }else if(sub_menu_name){
            breadcrumb = <span class="breadcrumb-item active">{sub_menu_name}</span>
        }else{
            breadcrumb = ""
        }

        if(sub_menu_function){
            breadcrumb_function = <span class="breadcrumb-item active">{sub_menu_function}</span>
        }

        if(main_menu_link){
            main_menu = <a href={main_menu_link} class="breadcrumb-item p-0">{main_menu_name}</a>
        }else{
            main_menu = `${main_menu_name}`
        }

        return (
            <div class="page-header page-header-light">
                <div class="page-header-content header-elements-md-inline">
                    <div class="page-title d-flex">
                        <h4>
                            <span class="font-weight-semibold">{sub_menu_name ? sub_menu_name : main_menu_name}</span>{sub_menu_function ? " - " + sub_menu_function : ""}
                        </h4>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                </div>
    
                <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                    <div class="d-flex">
                        <div class="breadcrumb">
                            <div class="breadcrumb-item"><i class="icon-home2 mr-2"></i> {main_menu}</div>
                            {breadcrumb}{breadcrumb_function}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
