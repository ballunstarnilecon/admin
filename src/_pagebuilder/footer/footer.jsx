import React, { Component } from "react";

export class Footer extends Component {
  render() {
    return (
      <div class="navbar navbar-expand-lg navbar-light">
				<div class="text-center d-lg-none w-100">
					<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
						<i class="icon-unfold mr-2"></i>
						Footer
					</button>
				</div>

				<div class="navbar-collapse collapse" id="navbar-footer">
					<span class="navbar-text">
						&copy; 2020. Nilecon (T)Thailand. Ltd. All Rights Copyright © 2020 by Nilecon : Develop By Chalisa Nowkaew
					</span>
				</div>
			</div>
    );
  }
}
