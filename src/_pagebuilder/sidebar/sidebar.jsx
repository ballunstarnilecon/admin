import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { adminAction } from "../../_actions";

import "./sidebar.css";

export class Sidebar extends Component {
	constructor(props, context) {
		super(props, context);
	
		this.handleActive = this.handleActive.bind(this);
		
		this.state = {
			active: 'ManageMovie',
			subactive: '',
			role_embassy: localStorage.getItem("role-embassy")
		};
	
	}
	

	componentDidMount() {
		if(sessionStorage.getItem("active") == "ManageMovie" || sessionStorage.getItem("active") == null){
			sessionStorage.setItem("active", "ManageMovie");
		}
	}

	handleActive(menuItem,e) {
		this.setState({ active: '' });
		this.setState({ subactive: '' });
		this.setState({ active: menuItem });

		sessionStorage.setItem("active", "");
		sessionStorage.setItem("subactive", "");
		sessionStorage.setItem("active", menuItem);
	}

	handleActiveSubmenu(menuItem) {
		this.setState({ subactive: '' });
		this.setState({ subactive: menuItem });

		sessionStorage.setItem("subactive", "");
		sessionStorage.setItem("subactive", menuItem);
	}


	render() {

		const activeStyle = 'nav-link active'
		const inActiveStyle = 'nav-link-inactive'
		const activeOpen = 'nav-item-open'
		const displaySubItem = { display: 'block' };
		const hideSubItem = { display: 'none' };

		var menu_active = sessionStorage.getItem("active");
		var menu_subactive = sessionStorage.getItem("subactive");

		console.log(sessionStorage.getItem("active"));

    return (


		<div className="sidebar sidebar-dark sidebar-main sidebar-expand-md">

			<div className="sidebar-mobile-toggler text-center">
				<a to="#" className="sidebar-mobile-main-toggle">
					<i className="icon-arrow-left8"></i>
				</a>
				Emabassy
				<a to="#" className="sidebar-mobile-expand">
					<i className="icon-screen-full"></i>
					<i className="icon-screen-normal"></i>
				</a>
			</div>


			<div className="sidebar-content">
				<div className="card card-sidebar-mobile">
					<ul className="nav nav-sidebar" data-nav-type="accordion">
						<li className="nav-item-header"><div className="text-uppercase font-size-xs line-height-xs">จัดการเมนู</div> <i className="icon-menu" title="Main"></i></li>

						<li className="nav-item">
							<NavLink to="/backoffice/movie" className={(menu_active == "ManageMovie" ? activeStyle : inActiveStyle)} onClick={this.handleActive.bind(this, 'ManageMovie')}>
								<i class="fas fa-solid fa-film"></i>
								<span>ภาพยนตร์</span>
							</NavLink>
						</li>

						<li className="nav-item">
							<NavLink to="/backoffice/roundmovie" className={(menu_active == "ManageRoundMovie" ? activeStyle : inActiveStyle)} onClick={this.handleActive.bind(this, 'ManageRoundMovie')}>
								<i class="fas fa-drum-steelpan"></i>
								<span>รอบภาพยนตร์</span>
							</NavLink>
						</li>

						<li className="nav-item">
							<NavLink to="/backoffice/banner" className={(menu_active == "ManageBanner" ? activeStyle : inActiveStyle)} onClick={this.handleActive.bind(this, 'ManageBanner')}>
								<i class="far fa-file-image"></i>
								<span>แบนเนอร์</span>
							</NavLink>
						</li>

						<li className="nav-item">
							<NavLink to="/backoffice/promotion" className={(menu_active == "ManagePromotion" ? activeStyle : inActiveStyle)} onClick={this.handleActive.bind(this, 'ManagePromotion')}>
								<i class="far fa-newspaper"></i>
								<span>โปรโมชัน</span>
							</NavLink>
						</li>

						<li className="nav-item">
							<NavLink to="/backoffice/news" className={(menu_active == "ManageNews" ? activeStyle : inActiveStyle)} onClick={this.handleActive.bind(this, 'ManageNews')}>
								<i class="fas fa-bullhorn"></i>
								<span>ข่าวสาร</span>
							</NavLink>
						</li>

						<li className="nav-item" style={{display:(this.state.role_embassy == "Admin" ? "block" : "none")}}>
							<NavLink to="/backoffice/admin" className={(menu_active == "ManageAdmin" ? activeStyle : inActiveStyle)} onClick={this.handleActive.bind(this, 'ManageAdmin')}>
								<i className="fas fa-user"></i>
								<span>จัดการผู้ใช้ระบบ</span>
							</NavLink>
						</li>

					</ul>
				</div>

			</div>

			
		</div>
    );
  }
}
